DROP TABLE servicos CASCADE CONSTRAINTS PURGE;
DROP TABLE custos_servicos CASCADE CONSTRAINTS PURGE;
DROP TABLE pontos_turisticos CASCADE CONSTRAINTS PURGE;
DROP TABLE itinerarios_viagens CASCADE CONSTRAINTS PURGE;
DROP TABLE viagens CASCADE CONSTRAINTS PURGE;

--Criar tabela servi�os 

create table servicos (
    cod_servico INTEGER CONSTRAINT pk_servicos_cod_servico PRIMARY KEY
                        CONSTRAINT nn_servicos_cod_servico NOT NULL,
    descricao VARCHAR(50) CONSTRAINT nn_servicos_descricao NOT NULL
    );

--Criar tabela custos_servicos

create table custos_servicos (
    cod_servico INTEGER CONSTRAINT nn_custos_servicos_cod_servico NOT NULL,
    data_inicio_custo TIMESTAMP CONSTRAINT nn_custos_servicos_data_inicio_custo NOT NULL,
    data_fim_custo TIMESTAMP,
    preco_base DECIMAL CONSTRAINT nn_custos_servicos_preco_base NOT NULL,
    custo_minuto DECIMAL CONSTRAINT nn_custos_servicos_custo_minuto NOT NULL,
    custo_km DECIMAL CONSTRAINT nn_custos_servicos_custo_km NOT NULL,
    taxa_IVA DECIMAL CONSTRAINT nn_custos_servicos_taxa_IVA NOT NULL,
    tempo_maximo_espera_minutos INTEGER CONSTRAINT nn_custos_servicos_tempo_maximo_espera_minutos NOT NULL,
    custo_espera_minuto DECIMAL CONSTRAINT nn_custos_servicos_custo_espera_minuto NOT NULL,
    custo_cancelamento_pedido DECIMAL CONSTRAINT nn_custos_servicos_custo_cancelamento_pedido NOT NULL,
    data_atualizacao TIMESTAMP,
    percentagem_atualizacao INTEGER,
    
    CONSTRAINT pk_custos_servicos_cod_servico_data_inicio_custo PRIMARY KEY(cod_servico, data_inicio_custo)
    );
    
--Altera��o da tabela para inser��o de chave estrangeira(cod_cd)    
ALTER TABLE custos_servicos ADD CONSTRAINT fk_custos_servicos_cod_servico FOREIGN KEY(cod_servico) REFERENCES servicos(cod_servico);

--Cria��o da tabela pontos_turisticos

create table pontos_turisticos (
    cod_ponto_turistico INTEGER CONSTRAINT pk_pontos_turisticos_cod_ponto_turistico PRIMARY KEY,
    nome_ponto_turistico VARCHAR(20) CONSTRAINT nn_pontos_turisticos_nome_ponto_turistico NOT NULL,
    tipo_ponto_turistico VARCHAR(2) CONSTRAINT ck_pontos_turisticos_tipo_ponto_turistico CHECK(tipo_ponto_turistico = 'M' OR tipo_ponto_turistico = 'MU' OR tipo_ponto_turistico = 'PN' OR tipo_ponto_turistico = 'MI')
    );

create table viagens (
    cod_viagem INTEGER CONSTRAINT pk_viagens_cod_viagem PRIMARY KEY
                       CONSTRAINT nn_viagens_cod_viagem NOT NULL,
    cod_pedido INTEGER CONSTRAINT nn_viagens_cod_pedido NOT NULL,
    atraso_passageiro_minutos INTEGER CONSTRAINT nn_viagens_atraso_passageiro_minutos NOT NULL
                                      CONSTRAINT ck_viagens_atraso_passageiro_minutos CHECK(atraso_passageiro_minutos >= 0),
    duracao_minutos INTEGER CONSTRAINT nn_viagens_duracao_minutos NOT NULL
                            CONSTRAINT ck_viagens_duracao_minutos CHECK(duracao_minutos >= 0)
    );
    
ALTER TABLE viagens ADD CONSTRAINT cod_pedido FOREIGN KEY(cod_pedido) REFERENCES pedidos_viagens(cod_pedido);

create table itinerarios_viagens (
    cod_viagem INTEGER CONSTRAINT nn_itinerarios_viagens_cod_viagem NOT NULL,
    cod_ponto_turistico INTEGER CONSTRAINT nn_itinerarios_viagens_cod_ponto_turistico NOT NULL, 
    hora_passagem TIMESTAMP CONSTRAINT nn_itinerarios_viagens_hora_passagem NOT NULL,
    
    CONSTRAINT pk_itinerarios_viagens_cod_viagem_cod_ponto_turistico PRIMARY KEY(cod_viagem, cod_ponto_turistico)
    );
    
ALTER TABLE itinerarios_viagens ADD CONSTRAINT fk_itinerarios_viagens_cod_viagem FOREIGN KEY(cod_viagem) REFERENCES viagens(cod_viagem);
ALTER TABLE itinerarios_viagens ADD CONSTRAINT fk_itinerarios_viagens_cod_ponto_turistico FOREIGN KEY(cod_ponto_turistico) REFERENCES pontos_turisticos(cod_ponto_turistico);


