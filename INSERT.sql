-- ** inserir dados nas tabelas **

-- ## tabela Veiculos ##
INSERT INTO Veiculos(matricula, marca, modelo, nr_chassis, data_matricula)
     VALUES ('11-AZ-23', 'Audi', 'A1', '9BGRD08X04G117974', TO_DATE('2005-11-14', 'yyyy-mm-dd'));
INSERT INTO Veiculos(matricula, marca, modelo, nr_chassis, data_matricula)
     VALUES ('11-SD-12', 'Toyota', 'Yaris', '9BGRD08X34G3S7974', TO_DATE('2012-10-02', 'yyyy-mm-dd'));
INSERT INTO Veiculos(matricula, marca, modelo, nr_chassis, data_matricula)
     VALUES ('13-SF-99', 'Toyota', 'Corolla', '9BPTD28F04G115974', TO_DATE('2018-12-23', 'yyyy-mm-dd'));
INSERT INTO Veiculos(matricula, marca, modelo, nr_chassis, data_matricula)
     VALUES ('11-45-QL', 'Opel', 'Corsa', '9BGRD08X04G123461', TO_DATE('2000-11-01', 'yyyy-mm-dd'));
INSERT INTO Veiculos(matricula, marca, modelo, nr_chassis, data_matricula)
     VALUES ('11-AL-45', 'Bugatti', 'Chiron', '9BGRD08X04G234534', TO_DATE('2014-11-14', 'yyyy-mm-dd'));
     
-- ## tabela Veiculos Condutores ##
INSERT INTO Veiculos_Condutores(matricula, nr_idCivil, data_inicio, data_fim)
    VALUES('11-AZ-23', '213254345', TO_DATE('2005-11-14', 'yyyy-mm-dd'), TO_DATE('2009-11-14', 'yyyy-mm-dd'));
INSERT INTO Veiculos_Condutores(matricula, nr_idCivil, data_inicio, data_fim)
    VALUES('11-SD-12', '219373564', TO_DATE('2012-12-14', 'yyyy-mm-dd'), TO_DATE('2018-11-25', 'yyyy-mm-dd'));
INSERT INTO Veiculos_Condutores(matricula, nr_idCivil, data_inicio, data_fim)
    VALUES('13-SF-99', '267890657', TO_DATE('2018-12-27', 'yyyy-mm-dd'), TO_DATE('2019-08-10', 'yyyy-mm-dd'));
INSERT INTO Veiculos_Condutores(matricula, nr_idCivil, data_inicio, data_fim)
    VALUES('11-45-QL', '256436825', TO_DATE('2003-11-14', 'yyyy-mm-dd'), TO_DATE('2016-06-30', 'yyyy-mm-dd'));
INSERT INTO Veiculos_Condutores(matricula, nr_idCivil, data_inicio, data_fim)
    VALUES('11-AL-45', '197564321', TO_DATE('2015-11-14', 'yyyy-mm-dd'), TO_DATE('2018-01-13', 'yyyy-mm-dd'));
    
-- ## tabela Condutores ##
INSERT INTO Condutores(nr_idCivil, nr_idCivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao)
    VALUES('213254345', NULL, 'Alexandre Leite', TO_DATE('1980-01-23', 'yyyy-mm-dd'), 'AV-234543 7',TO_DATE('2025-03-24', 'yyyy-mm-dd'));
INSERT INTO Condutores(nr_idCivil, nr_idCivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao)
    VALUES('219373564', '213254345', 'Jos� Guedes', TO_DATE('1987-04-21', 'yyyy-mm-dd'), 'AV-123123 5',TO_DATE('2022-04-21', 'yyyy-mm-dd'));
INSERT INTO Condutores(nr_idCivil, nr_idCivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao)
    VALUES('267890657', '213254345', 'Albertino Mendes', TO_DATE('1966-01-30', 'yyyy-mm-dd'), 'AV-854567 7',TO_DATE('2021-01-12', 'yyyy-mm-dd'));
INSERT INTO Condutores(nr_idCivil, nr_idCivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao)
    VALUES('256436825', NULL, 'Joana Lopes', TO_DATE('1992-10-12', 'yyyy-mm-dd'), 'AV-179546 2',TO_DATE('2024-01-25', 'yyyy-mm-dd'));
INSERT INTO Condutores(nr_idCivil, nr_idCivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao)
    VALUES('197564321', '256436825', 'Alberto Reis', TO_DATE('1980-01-23', 'yyyy-mm-dd'), 'AV-134253 7',TO_DATE('2025-03-24', 'yyyy-mm-dd'));

-- ## tabela Viagens ##
INSERT INTO viagens(cod_viagem, cod_pedido, atraso_passageiro_minutos, duracao_minutos)
    VALUES('0097', '101', '2', '20');
INSERT INTO viagens(cod_viagem, cod_pedido, atraso_passageiro_minutos, duracao_minutos)
    VALUES('0099', '103', '3', '25');
INSERT INTO viagens(cod_viagem, cod_pedido, atraso_passageiro_minutos, duracao_minutos)
    VALUES('0103', '88', '0', '16');
INSERT INTO viagens(cod_viagem, cod_pedido, atraso_passageiro_minutos, duracao_minutos)
    VALUES('0105', '111', '10', '12');
INSERT INTO viagens(cod_viagem, cod_pedido, atraso_passageiro_minutos, duracao_minutos)
    VALUES('0095', '99', '5', '10');
    
-- ## tabela Pedidos_Viagens ##
INSERT INTO pedidos_viagens(matricula, nr_idCivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    VALUES('11-AL-45', 197564321, TO_DATE('2015-11-14', 'yyyy-mm-dd'), '3', TO_TIMESTAMP('2019-10-05 13:43', 'yyyy-mm-dd hh24:mi'), TO_TIMESTAMP('2019-10-05 13:45', 'YYYY-MM-DD HH24:MI'), 13, 'N');
INSERT INTO pedidos_viagens(matricula, nr_idCivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    VALUES('11-SD-12', 219373564, TO_DATE('2012-12-14', 'yyyy-mm-dd'), '2', TO_TIMESTAMP('2019-09-13 20:45', 'yyyy-mm-dd hh24:mi'), TO_TIMESTAMP('2019-09-13 20:48', 'yyyy-mm-dd hh24:mi'), 9, 'N');
INSERT INTO pedidos_viagens(matricula, nr_idCivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    VALUES('13-SF-99', 267890657, TO_DATE('2012-12-14', 'yyyy-mm-dd'), '2', TO_TIMESTAMP('2019-09-02 09:35', 'yyyy-mm-dd hh24-mi'), TO_TIMESTAMP('2019-09-13 09:35', 'yyyy-mm-dd hh24-mi'), 11, 'N');
INSERT INTO pedidos_viagens(matricula, nr_idCivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    VALUES('11-45-QL', 256436825, TO_DATE('2003-11-14', 'yyyy-mm-dd'), '1', TO_TIMESTAMP('2019-08-29 12:11', 'yyyy-mm-dd hh24-mi'), TO_TIMESTAMP('2019-08-29 12:21', 'YYYY-MM-DD HH24-MI'), 6, 'N');
INSERT INTO pedidos_viagens(matricula, nr_idCivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado)
    VALUES('11-AZ-23', 213254345, TO_DATE('2005-11-14', 'yyyy-mm-dd'), '1', TO_TIMESTAMP('2019-11-22 17:48', 'yyyy-mm-dd hh24-mi'), TO_TIMESTAMP('2019-11-22 17:53', 'yyyy-mm-dd hh24-mi'), 5, 'N');
    
--## tabela Sevicos ##
INSERT INTO servicos(cod_servico, descricao)
    VALUES('1', 'Servi�o um');
INSERT INTO servicos(cod_servico, descricao)
    VALUES('2', 'Servi�o dois');
INSERT INTO servicos(cod_servico, descricao)
    VALUES('3', 'Servi�o tr�s');

--## tabela Pontos Turisticos ##
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000001,'Museu 1','M');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000002,'Museu 2','M');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000003,'Museu 3','M');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000004,'Museu 4','M');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000005,'Monumento 5','MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000006,'Monumento 6','MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000007,'Monumento 7','MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000008,'Monumento 8','MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico)    
    VALUES (00000009,'Parque Nacional 9','PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (000000010,'Parque Nacional 10','PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000011,'Parque Nacional 11','PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000012,'Parque Nacional 12','PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000013,'Miradouro 13','MI');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000014,'Miradouro 14','MI');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000015,'Miradouro 15','MI');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000016,'Miradouro 16','MI');

--## tabela Custos Servi�os ##
INSERT INTO custos_servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, data_atualizacao, percentagem_atualizacao) 
    VALUES('0000000001',TO_DATE('2017-10-04','yyyy-mm-dd'), TO_DATE('2019-12-01','yyyy-mm-dd'), 105.2, 1.35, 0.75, 23.0, 30, 1.0, 55.1, NULL, 0);
INSERT INTO custos_servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, data_atualizacao, percentagem_atualizacao) 
    VALUES('0000000002',TO_DATE('2018-10-04','yyyy-mm-dd'), TO_DATE('2018-12-01','yyyy-mm-dd'), 120.2, 1.45, 1.50, 23.0, 25, 2.0, 60.1, NULL, 0);
INSERT INTO custos_servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, data_atualizacao, percentagem_atualizacao) 
    VALUES('0000000003',TO_DATE('2018-01-04','yyyy-mm-dd'), TO_DATE('2018-02-01','yyyy-mm-dd'), 45.0, 1.85, 0.25, 23.0, 45, 3.0, 22.3, NULL, 0);
INSERT INTO custos_servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, data_atualizacao, percentagem_atualizacao) 
    VALUES('0000000004',TO_DATE('2019-06-04','yyyy-mm-dd'), TO_DATE('2019-12-01','yyyy-mm-dd'), 180.8, 1.45, 1.20, 23.0, 60, 1.5, 80.0, NULL, 0);

--## tabela Itinerarios Viagens ##
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (0001,0002, TO_TIMESTAMP('2018-01-05 15:10', 'YYYY-MM-DD HH24:MI'));
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (0001,0003, TO_TIMESTAMP('2018-01-05 15:11', 'YYYY-MM-DD HH24:MI'));
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (0001,0004, TO_TIMESTAMP('2018-01-05 15:12', 'YYYY-MM-DD HH24:MI'));
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (0002,0003, TO_TIMESTAMP('2018-01-05 15:13', 'YYYY-MM-DD HH24:MI'));
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (0002,0005, TO_TIMESTAMP('2019-02-10 16:30', 'YYYY-MM-DD HH24:MI'));
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem)   
    VALUES (0002,0010, TO_TIMESTAMP('2019-09-10 16:35', 'YYYY-MM-DD HH24:MI'));
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (0002,0015, TO_TIMESTAMP('2019-08-10 16:40', 'YYYY-MM-DD HH24:MI'));
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (0003,0006, TO_TIMESTAMP('2017-06-05 17:45', 'YYYY-MM-DD HH24:MI'));
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (0004,0010, TO_TIMESTAMP('2018-05-05 18:25', 'YYYY-MM-DD HH24:MI'));
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (0005,0016, TO_TIMESTAMP('2018-08-10 09:30', 'YYYY-MM-DD HH24:MI'));
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (0006,0016, TO_TIMESTAMP('2018-12-05 08:30', 'YYYY-MM-DD HH24:MI'));
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (0007,0005, TO_TIMESTAMP('2019-05-14 20:30', 'YYYY-MM-DD HH24:MI'));
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (0008,0009, TO_TIMESTAMP('2019-05-17 22:35', 'YYYY-MM-DD HH24:MI'));
