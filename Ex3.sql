create or replace function funcObterInfoSemanalVeiculos(dat DATE) return sys_refcursor is
c sys_refcursor;
dataInicio DATE := obterDiaInicial(dat);
dataFim DATE := obterDiaFinal(dat) + 1;
begin
    open c for 
        select v.matricula, dataInicio, dataFim, 
    (select count(*) from Pedidos_viagens where matricula = v.matricula and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as numero_viagens, 
    (select sum(distancia_km) from Pedidos_viagens where matricula=v.matricula and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as soma_km,
    (select sum(duracao_minutos) from Pedidos_viagens pv inner join Viagens vi on vi.cod_pedido = pv.cod_pedido 
                                            where v.matricula = pv.matricula
    and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as soma_duracao
    from Pedidos_viagens pv inner join Veiculos v
    on pv.matricula = v.matricula
    inner join Viagens vi
    on pv.cod_pedido = vi.cod_pedido
    group by v.matricula;    
return c;
end;
/

create or replace function obterDiaInicial(dat DATE) return DATE is
dataInicio DATE;
begin
Select TRUNC(dat, 'IW') TO_DATE into dataInicio from dual;
return dataInicio;
end;
/

create or replace function obterDiaFinal(dat DATE) return DATE is
dataFim DATE;
begin
Select NEXT_DAY(TRUNC(dat,'IW'),'Domingo') TO_DATE into dataFim from dual;
return dataFim;
end;
/

--Blocos an�nimos
set serveroutput on;
declare
cursor_semanal SYS_REFCURSOR := funcObterInfoSemanalVeiculos(TO_DATE('2019-10-05 13:45', 'YYYY-MM-DD HH24:MI'));
m Veiculos.matricula%type;
d1 DATE;
d2 DATE;
c1 int;
c2 int;
c3 int;

begin
    dbms_output.put_line('Estat�sticas semanais dos ve�culos: ');
    dbms_output.put_line('');
    loop
        fetch cursor_semanal into m, d1, d2, c1, c2, c3;
        exit when cursor_semanal%notfound;
        dbms_output.put_line('Matricula: ' || m || ' Data de in�cio: ' || d1 || ' Data de fim: ' || d2 || ' Numero de viagens: ' || c1 || ' Numero de quil�metros: ' || c2 || ' Dura��o (minutos): ' || c3);
        dbms_output.put_line('');
    end loop;
end;
/

set serveroutput on;
declare
dataInicio DATE := obterDiaInicial(TO_DATE('2019-10-05 13:45', 'YYYY-MM-DD HH24:MI'));
begin
    dbms_output.put_line(dataInicio);
end;
/

set serveroutput on;
declare
dataFim DATE := obterDiaFinal(TO_DATE('2019-10-05 13:45', 'YYYY-MM-DD HH24:MI'));
begin
        dbms_output.put_line(dataFim);
end;
/
