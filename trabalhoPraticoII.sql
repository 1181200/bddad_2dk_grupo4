--1--
create or replace function funcTopServico (codServico servicos.cod_servico%type, dataInicio veiculos_Condutores.data_inicio%type,
dataFim veiculos_condutores.data_fim%type , n INTEGER) return sys_refcursor
is
    id_c_rc sys_refcursor;
    contador integer;

begin
    select count(*) into contador
    from servicos s
    where s.cod_servico = codServico;
if (contador = 0 or dataInicio > dataFim)
    then
    OPEN id_c_rc FOR  
        select *
        from servicos
        where 1 = 2;
else
    OPEN id_c_rc FOR
        with aux as (
             select c.nr_idCivil, SUM((CS.preco_base + CS.custo_minuto*v.duracao_minutos) + (CS.custo_km * PV.distancia_km)
                + (CS.custo_espera_minuto * V.atraso_passageiro_minutos)) custo
            FROM condutores c
            INNER JOIN pedidos_viagens pv ON pv.nr_idCivil = c.nr_idCivil
            INNER JOIN custos_servicos cs ON cs.cod_servico = pv.cod_servico
            INNER JOIN viagens v ON v.cod_pedido = pv.cod_pedido
            WHERE pv.data_hora_recolha_passageiro > dataInicio and pv.data_hora_recolha_passageiro < dataFim and codServico = pv.cod_servico
            GROUP BY c.nr_idCivil
            order by 2 desc
        )

        SELECT nr_idCivil
        FROM aux
        FETCH FIRST n ROWS ONLY;
end if;
    return (id_c_rc);
end; 

--2--

create or replace function funcSobreposicoesVeiculosCondutores return boolean is
sobreposicoes_count int;
begin
    select count(*) into sobreposicoes_count
    from veiculos_condutores  vc
    where exists( select  1
                  from veiculos_condutores 
                  where matricula=vc.matricula and 
                  ((data_inicio < vc.data_inicio and data_fim is null)
            or
            (data_inicio > vc.data_inicio and data_fim < vc.data_fim)
            or 
            (data_inicio < vc.data_inicio and data_fim > vc.data_inicio) 
            or
            (data_inicio < vc.data_fim and data_fim > vc.data_fim)
            or
            (data_inicio < vc.data_fim and vc.data_fim is null))); --data_fim2 depois de data_fim1 mas data_inicio2 antes de data_fim1
    return sobreposicoes_count>0;
end;

--ex3
create or replace function funcObterInfoSemanalVeiculos(dat DATE) return sys_refcursor is
c sys_refcursor;
dataInicio DATE := obterDiaInicial(dat);
dataFim DATE := obterDiaFinal(dat) + 1;
begin
    open c for 
        select v.matricula, dataInicio, dataFim, 
    (select count(*) from Pedidos_viagens where matricula = v.matricula and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as numero_viagens, 
    (select sum(distancia_km) from Pedidos_viagens where matricula=v.matricula and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as soma_km,
    (select sum(duracao_minutos) from Pedidos_viagens pv inner join Viagens vi on vi.cod_pedido = pv.cod_pedido 
                                            where v.matricula = pv.matricula
    and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as soma_duracao
    from Pedidos_viagens pv inner join Veiculos v
    on pv.matricula = v.matricula
    inner join Viagens vi
    on pv.cod_pedido = vi.cod_pedido
    group by v.matricula;    
return c;
end;
/

create or replace function obterDiaInicial(dat DATE) return DATE is
dataInicio DATE;
begin
Select TRUNC(dat, 'IW') TO_DATE into dataInicio from dual;
return dataInicio;
end;
/

create or replace function obterDiaFinal(dat DATE) return DATE is
dataFim DATE;
begin
Select NEXT_DAY(TRUNC(dat,'IW'),'Domingo') TO_DATE into dataFim from dual;
return dataFim;
end;
/

--Blocos an�nimos
set serveroutput on;
declare
cursor_semanal SYS_REFCURSOR := funcObterInfoSemanalVeiculos(TO_DATE('2019-10-05 13:45', 'YYYY-MM-DD HH24:MI'));
m Veiculos.matricula%type;
d1 DATE;
d2 DATE;
c1 int;
c2 int;
c3 int;

begin
    dbms_output.put_line('Estat�sticas semanais dos ve�culos: ');
    dbms_output.put_line('');
    loop
        fetch cursor_semanal into m, d1, d2, c1, c2, c3;
        exit when cursor_semanal%notfound;
        dbms_output.put_line('Matricula: ' || m || ' Data de in�cio: ' || d1 || ' Data de fim: ' || d2 || ' Numero de viagens: ' || c1 || ' Numero de quil�metros: ' || c2 || ' Dura��o (minutos): ' || c3);
        dbms_output.put_line('');
    end loop;
end;
/

set serveroutput on;
declare
dataInicio DATE := obterDiaInicial(TO_DATE('2019-10-05 13:45', 'YYYY-MM-DD HH24:MI'));
begin
    dbms_output.put_line(dataInicio);
end;
/

set serveroutput on;
declare
dataFim DATE := obterDiaFinal(TO_DATE('2019-10-05 13:45', 'YYYY-MM-DD HH24:MI'));
begin
        dbms_output.put_line(dataFim);
end;
/
--ex3
create or replace function funcObterInfoSemanalVeiculos(dat DATE) return sys_refcursor is
c sys_refcursor;
dataInicio DATE := obterDiaInicial(dat);
dataFim DATE := obterDiaFinal(dat) + 1;
begin
    open c for 
        select v.matricula, dataInicio, dataFim, 
    (select count(*) from Pedidos_viagens where matricula = v.matricula and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as numero_viagens, 
    (select sum(distancia_km) from Pedidos_viagens where matricula=v.matricula and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as soma_km,
    (select sum(duracao_minutos) from Pedidos_viagens pv inner join Viagens vi on vi.cod_pedido = pv.cod_pedido 
                                            where v.matricula = pv.matricula
    and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as soma_duracao
    from Pedidos_viagens pv inner join Veiculos v
    on pv.matricula = v.matricula
    inner join Viagens vi
    on pv.cod_pedido = vi.cod_pedido
    group by v.matricula;    
return c;
end;
/

create or replace function obterDiaInicial(dat DATE) return DATE is
dataInicio DATE;
begin
Select TRUNC(dat, 'IW') TO_DATE into dataInicio from dual;
return dataInicio;
end;
/

create or replace function obterDiaFinal(dat DATE) return DATE is
dataFim DATE;
begin
Select NEXT_DAY(TRUNC(dat,'IW'),'Domingo') TO_DATE into dataFim from dual;
return dataFim;
end;
/

--Blocos an�nimos
set serveroutput on;
declare
cursor_semanal SYS_REFCURSOR := funcObterInfoSemanalVeiculos(TO_DATE('2019-10-05 13:45', 'YYYY-MM-DD HH24:MI'));
m Veiculos.matricula%type;
d1 DATE;
d2 DATE;
c1 int;
c2 int;
c3 int;

begin
    dbms_output.put_line('Estat�sticas semanais dos ve�culos: ');
    dbms_output.put_line('');
    loop
        fetch cursor_semanal into m, d1, d2, c1, c2, c3;
        exit when cursor_semanal%notfound;
        dbms_output.put_line('Matricula: ' || m || ' Data de in�cio: ' || d1 || ' Data de fim: ' || d2 || ' Numero de viagens: ' || c1 || ' Numero de quil�metros: ' || c2 || ' Dura��o (minutos): ' || c3);
        dbms_output.put_line('');
    end loop;
end;
/

set serveroutput on;
declare
dataInicio DATE := obterDiaInicial(TO_DATE('2019-10-05 13:45', 'YYYY-MM-DD HH24:MI'));
begin
    dbms_output.put_line(dataInicio);
end;
/

set serveroutput on;
declare
dataFim DATE := obterDiaFinal(TO_DATE('2019-10-05 13:45', 'YYYY-MM-DD HH24:MI'));
begin
        dbms_output.put_line(dataFim);
end;
/


--4-- 
create or replace function melhorServico return servicos.cod_servico%type
is
    servico servicos.cod_servico%type;

begin
    select servicox INTO servico
    FROM (select s.cod_servico servicox, SUM((CS.preco_base + CS.custo_minuto*v.duracao_minutos) + (CS.custo_km * PV.distancia_km)
                + (CS.custo_espera_minuto * V.atraso_passageiro_minutos)) custo
    FROM servicos s
    INNER JOIN pedidos_viagens pv ON s.cod_servico = pv.cod_servico
    INNER JOIN viagens v ON v.cod_pedido = pv.cod_pedido
    INNER JOIN custos_servicos cs ON cs.cod_servico = pv.cod_servico
    WHERE pv.data_hora_recolha_passageiro > ADD_MONTHS(SYSDATE,-12)
    GROUP BY s.cod_servico
    order by 2 DESC
    FETCH FIRST 1 ROWS ONLY);
    
    return (servico);
end melhorServico;


create or replace procedure atualizaDataPerc(x INTEGER, data_at timestamp, codServico custos_servicos.cod_servico%type) is
begin
    SAVEPOINT sp;
    update custos_servicos set custos_servicos.data_atualizacao = data_at where custos_servicos.cod_servico = codServico;
    update custos_servicos set custos_servicos.percentagem_atualizacao = x where custos_servicos.cod_servico = codServico;
    COMMIT;
end atualizaDataPerc;

set serveroutput on;
create or replace procedure procAtualizarCustosServico2(x integer, dataAtualizacao date) is
codServico servicos.cod_servico%type := melhorServico;
lastUp TIMESTAMP;
precoBase char;
custoMinuto char;
custoEspera char;
custoCancelamento char;
dataAt date := dataAtualizacao;

begin
    SELECT SCN_TO_TIMESTAMP(MAX(ora_rowscn)) INTO lastUp
    FROM custos_servicos cs
    WHERE cs.cod_servico = melhorServico;
    
    if(dataAtualizacao is null)
        then dataAt := SYSDATE;
    end if;
    
    if (lastUp < ADD_MONTHS(dataAtualizacao,-6))
    then
    --    atualizaDataPerc(x, dataAtualizacao, codServico);
    
    update custos_servicos set custos_servicos.preco_base=custos_servicos.preco_base*((100+x)/100) where custos_servicos.cod_servico = codServico;
    update custos_servicos set custos_servicos.custo_minuto=custos_servicos.custo_minuto*((100+x)/100) where custos_servicos.cod_servico = codServico;
    update custos_servicos set custos_servicos.custo_espera_minuto=custos_servicos.custo_espera_minuto*((100+x)/100) where custos_servicos.cod_servico = codServico;
    update custos_servicos set custos_servicos.custo_cancelamento_pedido=custos_servicos.custo_cancelamento_pedido*((100+x)/100) where custos_servicos.cod_servico = codServico;
    
    COMMIT;
    
    select TO_CHAR(cs.preco_base), TO_CHAR(cs.custo_minuto), TO_CHAR(cs.custo_espera_minuto), TO_CHAR(cs.custo_cancelamento_pedido) INTO precoBase, custoMinuto, custoEspera, custoCancelamento
    from custos_servicos cs;
    
    dbms_output.put_line('Pre�os ir�o ser atualizados! Pre�o base:' || precoBase || 'Custo minuto:' || custoMinuto || 'Custo de espera minuto:' || custoEspera || 'Custo de cancelamento do pedido:' || custocancelamento);
    else
        dbms_output.put_line('Erro! Pre�os alterados h� menos de 6 meses!');
    end if;
end procAtualizarCustosServico2;

--- SEM PERMISS�ES ---

create or replace trigger
   atualizarPrecos
after startup on database
DECLARE
    CURSOR custos IS
        SELECT cod_servico, data_atualizacao dataA, percentagem_atualizacao x
        FROM custos_servicos
        WHERE data_atualizacao IS NOT NULL;

begin
    FOR recs IN custos LOOP
        UPDATE custos_servicos
            SET preco_base = preco_base*((100+recs.x)/100), custo_minuto = custo_minuto*((100+recs.x)/100), custo_espera_minuto = custo_espera_minuto*((100+recs.x)/100),
            custo_cancelamento_pedido = custo_cancelamento_pedido*((100+recs.x)/100), percentagem_atualizacao = 0, data_atualizacao = null
            where (data_atualizacao = SYSDATE and recs.cod_servico = cod_servico);
            
    END LOOP;
end atualizarPrecos;   

--BLOCOS AN�NIMOS--

set SERVEROUTPUT ON;
DECLARE
    res sys_refcursor;
    codServico INTEGER := 3;
    dataInicio date := TO_DATE('2010-01-01', 'yyyy-mm-dd');
    dataFim date := TO_DATE('2010-01-05', 'yyyy-mm-dd');
    n INTEGER := 3;
    nrId INTEGER;
BEGIN
    res := funcTopServico(codServico, dataInicio, dataFim, n);
    dbms_output.put_line(rpad('Nr IdCivil', 13));
loop
    fetch res into data_inicio_sm, data_fim_sm, matricula, distancia, tempo, numero;
    exit when res%notfound;
    dbms_output.put_line(rpad(nrId, 13));
end loop;
END;
/

set SERVEROUTPUT ON;
DECLARE
    res integer;
BEGIN
    func_res := melhorServico;
    dbms_output.put_line("Id do melhor servi�o:" || res);
END;
/

set SERVEROUTPUT ON;
DECLARE
    dat date := TO_DATE('2021-01-01', 'YYYY-mm-dd');
    perc INTEGER := 20;
    
BEGIN
    procAtualizarCustosServico2(perc, dat);
    dbms_output.put_line("Id do melhor servi�o:" || res);
END;
/

--5--

create or replace procedure procDetetarAssociacoes is
m1 Veiculos_condutores.matricula%type;
id1 Veiculos_condutores.nr_Idcivil%type;
di1 date;
df1 date;
m2 Veiculos_condutores.matricula%type;
id2 Veiculos_condutores.nr_Idcivil%type;
di2 date;
df2 date;
maux Veiculos_condutores.matricula%type;
idaux Veiculos_condutores.nr_Idcivil%type;
diaux date;
dfaux date;
n int := 1;
csize int := 6;

cursor sc is
    select distinct vc1.* from veiculos_condutores vc1 join veiculos_condutores vc2 
    on vc1.nr_idcivil = vc2.nr_idcivil   
    where ((vc1.data_inicio < vc2.data_inicio and vc1.data_fim is null)
            or
            (vc1.data_inicio > vc2.data_inicio and vc1.data_fim < vc2.data_fim)
            or 
            (vc1.data_inicio < vc2.data_inicio and vc1.data_fim > vc2.data_inicio) 
            or
            (vc1.data_inicio < vc2.data_fim and vc1.data_fim > vc2.data_fim)
            or
            (vc1.data_inicio < vc2.data_fim and vc2.data_fim is null)
            or
            (vc1.data_inicio > vc2.data_inicio and vc1.data_fim is null))
    order by vc1.nr_idcivil, vc1.data_inicio asc ; 

begin

    open sc;
    while n < csize
        loop 
            maux := m2;
            idaux:= id2;
            diaux := di2;
            dfaux := df2;
            if n = 1 then 
                fetch sc into m1, id1, di1, df1;
                fetch sc into m2, id2, di2, df2;
                update veiculos_condutores set data_fim = di2 where data_inicio = di1;
            end if;
            if n > 1 then
                fetch sc into m2, id2, di2, df2;
                if (id2 = idaux) and (dfaux > di2 or (dfaux is null)) then
                    update veiculos_condutores set data_fim = di2 where data_inicio = diaux;
                    end if;
            end if;
            n := n+1;
        end loop;
        close sc;
end;
--ex 6
--Cria�ao da tabela ResumosVeiculos
CREATE TABLE ResumosVeiculos (
instante TIMESTAMP
CONSTRAINT nn_resumos_veiculos_instante NOT NULL,
data_inicio DATE
CONSTRAINT nn_resumos_veiculos_data_inicio NOT NULL,
data_fim DATE
CONSTRAINT nn_resumos_veiculos_data_fim NOT NULL,
matricula VARCHAR2(8)
CONSTRAINT nn_resumos_veiculos_matricula NOT NULL,
nr_viagens INTEGER
CONSTRAINT ck_resumos_veiculos_nr_viagens CHECK ( nr_viagens > 0 ),
soma_km INTEGER
CONSTRAINT ck_resumos_veiculos_soma_km CHECK ( soma_km > 0 ),
soma_duracao INTEGER
CONSTRAINT ck_resumos_veiculos_soma_duracao CHECK ( soma_duracao > 0 ),
CONSTRAINT ck_resumos_veiculos_data_fim CHECK ( data_fim > data_inicio ) ,
CONSTRAINT pk_resumos_veiculos_data_inicio_data_fim_matricula PRIMARY KEY (
matricula, data_inicio, data_fim )
);

--Fun�ao que obtem a informa�acao da semana, da data recebida por parametro, das viagens dos veiculos
create or replace function funcObterInfoSemanalVeiculos(dat DATE) return sys_refcursor is
c sys_refcursor;
dataInicio DATE := obterDiaInicial(dat);
dataFim DATE := obterDiaFinal(dat) + 1;
begin
    open c for 
        select v.matricula, dataInicio, dataFim, 
    (select count(*) from Pedidos_viagens where matricula = v.matricula and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as numero_viagens, 
    (select sum(distancia_km) from Pedidos_viagens where matricula=v.matricula and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as soma_km,
    (select sum(duracao_minutos) from Pedidos_viagens pv inner join Viagens vi on vi.cod_pedido = pv.cod_pedido 
                                            where v.matricula = pv.matricula
    and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as soma_duracao
    from Pedidos_viagens pv inner join Veiculos v
    on pv.matricula = v.matricula
    inner join Viagens vi
    on pv.cod_pedido = vi.cod_pedido
    group by v.matricula;    
return c;
end;
/

--Fun�ao que obtem a data do inicio de semana da data recebida por parametro
create or replace function obterDiaInicial(dat DATE) return DATE is
dataInicio DATE;
begin
Select TRUNC(dat, 'IW') TO_DATE into dataInicio from dual;
return dataInicio;
end;
/

--Fun�ao que obtem a data do fim de semana da data recebida por parametro
create or replace function obterDiaFinal(dat DATE) return DATE is
dataFim DATE;
begin
Select NEXT_DAY(TRUNC(dat,'IW'),'Domingo') TO_DATE into dataFim from dual;
return dataFim;
end;
/

--Procedimento que insere na tabela ResumoVeiculos a informa�ao da semana, da data recebida por parametro, das viagens dos veiculos
create or replace procedure procGuardarInformacaoSemanal(instante Timestamp) as
    data_inicio Date;
    data_fim Date;
    matricula veiculos.matricula%type;
    nr_viagens int;
    soma_km int;
    soma_duracao int;
    c SYS_REFCURSOR := funcObterInfoSemanalVeiculos(instante);
    numVeiculosTotal int;
    numVeiculosComViagens int := 0;
begin
    delete from ResumosVeiculos;
    select count(*) into numVeiculosTotal  from veiculos;
    begin
        loop
            fetch c into matricula, data_inicio, data_fim, nr_viagens, soma_km, soma_duracao;
            exit when c%notfound;
            if(nr_viagens>0) then
                numVeiculosComViagens := numVeiculosComViagens + 1;
                dbms_output.put_line(matricula);
                INSERT INTO ResumosVeiculos(instante, data_inicio, data_fim, matricula, nr_viagens, soma_km, soma_duracao)
                VALUES (instante, data_inicio, data_fim, matricula, nr_viagens, soma_km, soma_duracao);
            end if;
        end loop;
        close c;
        if(numVeiculosComViagens>0) then
            dbms_output.put_line('A percentagem de veiculos que efetuaram viagens nessa semana �: '|| (numVeiculosComViagens/numVeiculosTotal)*100 || '%');
        else 
            dbms_output.put_line('Nenhum veiculo efetuou viagens nessa semana logo a percentagem �: 0%');
        end if;
    end;
end;
/

--Blocos an�nimos
set serveroutput on;
declare
begin
    procGuardarInformacaoSemanal(TO_TIMESTAMP('2019-10-05 13:45', 'YYYY-MM-DD HH24:MI'));
    -- resultado esperado: o veiculo 11-AL-45 efetuou viagens nessa semana, percentagem: 20% pois existem 5 veiculos
end;
/

--Blocos an�nimos
set serveroutput on;
declare
begin
    procGuardarInformacaoSemanal(TO_TIMESTAMP('2019-09-13 20:48', 'yyyy-mm-dd hh24:mi'));
    -- resultado esperado: os veiculos 11-SD-12, 13-SF-99 efetuaram viagens nessa semana, percentagem: 40% pois existem 5 veiculos
end;
/

--Blocos an�nimos
set serveroutput on;
declare
begin
    procGuardarInformacaoSemanal(TO_TIMESTAMP('1980-03-13 10:48', 'yyyy-mm-dd hh24:mi'));
    -- resultado esperado: nenhum veiculo efetuou viagens nessa semana.
end;
/

--ex 6
--Cria�ao da tabela ResumosVeiculos
CREATE TABLE ResumosVeiculos (
instante TIMESTAMP
CONSTRAINT nn_resumos_veiculos_instante NOT NULL,
data_inicio DATE
CONSTRAINT nn_resumos_veiculos_data_inicio NOT NULL,
data_fim DATE
CONSTRAINT nn_resumos_veiculos_data_fim NOT NULL,
matricula VARCHAR2(8)
CONSTRAINT nn_resumos_veiculos_matricula NOT NULL,
nr_viagens INTEGER
CONSTRAINT ck_resumos_veiculos_nr_viagens CHECK ( nr_viagens > 0 ),
soma_km INTEGER
CONSTRAINT ck_resumos_veiculos_soma_km CHECK ( soma_km > 0 ),
soma_duracao INTEGER
CONSTRAINT ck_resumos_veiculos_soma_duracao CHECK ( soma_duracao > 0 ),
CONSTRAINT ck_resumos_veiculos_data_fim CHECK ( data_fim > data_inicio ) ,
CONSTRAINT pk_resumos_veiculos_data_inicio_data_fim_matricula PRIMARY KEY (
matricula, data_inicio, data_fim )
);

--Fun�ao que obtem a informa�acao da semana, da data recebida por parametro, das viagens dos veiculos
create or replace function funcObterInfoSemanalVeiculos(dat DATE) return sys_refcursor is
c sys_refcursor;
dataInicio DATE := obterDiaInicial(dat);
dataFim DATE := obterDiaFinal(dat) + 1;
begin
    open c for 
        select v.matricula, dataInicio, dataFim, 
    (select count(*) from Pedidos_viagens where matricula = v.matricula and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as numero_viagens, 
    (select sum(distancia_km) from Pedidos_viagens where matricula=v.matricula and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as soma_km,
    (select sum(duracao_minutos) from Pedidos_viagens pv inner join Viagens vi on vi.cod_pedido = pv.cod_pedido 
                                            where v.matricula = pv.matricula
    and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as soma_duracao
    from Pedidos_viagens pv inner join Veiculos v
    on pv.matricula = v.matricula
    inner join Viagens vi
    on pv.cod_pedido = vi.cod_pedido
    group by v.matricula;    
return c;
end;
/

--Fun�ao que obtem a data do inicio de semana da data recebida por parametro
create or replace function obterDiaInicial(dat DATE) return DATE is
dataInicio DATE;
begin
Select TRUNC(dat, 'IW') TO_DATE into dataInicio from dual;
return dataInicio;
end;
/

--Fun�ao que obtem a data do fim de semana da data recebida por parametro
create or replace function obterDiaFinal(dat DATE) return DATE is
dataFim DATE;
begin
Select NEXT_DAY(TRUNC(dat,'IW'),'Domingo') TO_DATE into dataFim from dual;
return dataFim;
end;
/

--Procedimento que insere na tabela ResumoVeiculos a informa�ao da semana, da data recebida por parametro, das viagens dos veiculos
create or replace procedure procGuardarInformacaoSemanal(instante Timestamp) as
    data_inicio Date;
    data_fim Date;
    matricula veiculos.matricula%type;
    nr_viagens int;
    soma_km int;
    soma_duracao int;
    c SYS_REFCURSOR := funcObterInfoSemanalVeiculos(instante);
    numVeiculosTotal int;
    numVeiculosComViagens int := 0;
begin
    delete from ResumosVeiculos;
    select count(*) into numVeiculosTotal  from veiculos;
    begin
        loop
            fetch c into matricula, data_inicio, data_fim, nr_viagens, soma_km, soma_duracao;
            exit when c%notfound;
            if(nr_viagens>0) then
                numVeiculosComViagens := numVeiculosComViagens + 1;
                dbms_output.put_line(matricula);
                INSERT INTO ResumosVeiculos(instante, data_inicio, data_fim, matricula, nr_viagens, soma_km, soma_duracao)
                VALUES (instante, data_inicio, data_fim, matricula, nr_viagens, soma_km, soma_duracao);
            end if;
        end loop;
        close c;
        if(numVeiculosComViagens>0) then
            dbms_output.put_line('A percentagem de veiculos que efetuaram viagens nessa semana �: '|| (numVeiculosComViagens/numVeiculosTotal)*100 || '%');
        else 
            dbms_output.put_line('Nenhum veiculo efetuou viagens nessa semana logo a percentagem �: 0%');
        end if;
    end;
end;
/

--Blocos an�nimos
set serveroutput on;
declare
begin
    procGuardarInformacaoSemanal(TO_TIMESTAMP('2019-10-05 13:45', 'YYYY-MM-DD HH24:MI'));
    -- resultado esperado: o veiculo 11-AL-45 efetuou viagens nessa semana, percentagem: 20% pois existem 5 veiculos
end;
/

--Blocos an�nimos
set serveroutput on;
declare
begin
    procGuardarInformacaoSemanal(TO_TIMESTAMP('2019-09-13 20:48', 'yyyy-mm-dd hh24:mi'));
    -- resultado esperado: os veiculos 11-SD-12, 13-SF-99 efetuaram viagens nessa semana, percentagem: 40% pois existem 5 veiculos
end;
/

--Blocos an�nimos
set serveroutput on;
declare
begin
    procGuardarInformacaoSemanal(TO_TIMESTAMP('1980-03-13 10:48', 'yyyy-mm-dd hh24:mi'));
    -- resultado esperado: nenhum veiculo efetuou viagens nessa semana.
end;
/






