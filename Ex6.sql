--Cria�ao da tabela ResumosVeiculos
CREATE TABLE ResumosVeiculos (
instante TIMESTAMP
CONSTRAINT nn_resumos_veiculos_instante NOT NULL,
data_inicio DATE
CONSTRAINT nn_resumos_veiculos_data_inicio NOT NULL,
data_fim DATE
CONSTRAINT nn_resumos_veiculos_data_fim NOT NULL,
matricula VARCHAR2(8)
CONSTRAINT nn_resumos_veiculos_matricula NOT NULL,
nr_viagens INTEGER
CONSTRAINT ck_resumos_veiculos_nr_viagens CHECK ( nr_viagens > 0 ),
soma_km INTEGER
CONSTRAINT ck_resumos_veiculos_soma_km CHECK ( soma_km > 0 ),
soma_duracao INTEGER
CONSTRAINT ck_resumos_veiculos_soma_duracao CHECK ( soma_duracao > 0 ),
CONSTRAINT ck_resumos_veiculos_data_fim CHECK ( data_fim > data_inicio ) ,
CONSTRAINT pk_resumos_veiculos_data_inicio_data_fim_matricula PRIMARY KEY (
matricula, data_inicio, data_fim )
);

--Fun�ao que obtem a informa�acao da semana, da data recebida por parametro, das viagens dos veiculos
create or replace function funcObterInfoSemanalVeiculos(dat DATE) return sys_refcursor is
c sys_refcursor;
dataInicio DATE := obterDiaInicial(dat);
dataFim DATE := obterDiaFinal(dat) + 1;
begin
    open c for 
        select v.matricula, dataInicio, dataFim, 
    (select count(*) from Pedidos_viagens where matricula = v.matricula and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as numero_viagens, 
    (select sum(distancia_km) from Pedidos_viagens where matricula=v.matricula and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as soma_km,
    (select sum(duracao_minutos) from Pedidos_viagens pv inner join Viagens vi on vi.cod_pedido = pv.cod_pedido 
                                            where v.matricula = pv.matricula
    and data_hora_pedido between dataInicio and dataFim and cancelado = 'N') as soma_duracao
    from Pedidos_viagens pv inner join Veiculos v
    on pv.matricula = v.matricula
    inner join Viagens vi
    on pv.cod_pedido = vi.cod_pedido
    group by v.matricula;    
return c;
end;
/

--Fun�ao que obtem a data do inicio de semana da data recebida por parametro
create or replace function obterDiaInicial(dat DATE) return DATE is
dataInicio DATE;
begin
Select TRUNC(dat, 'IW') TO_DATE into dataInicio from dual;
return dataInicio;
end;
/

--Fun�ao que obtem a data do fim de semana da data recebida por parametro
create or replace function obterDiaFinal(dat DATE) return DATE is
dataFim DATE;
begin
Select NEXT_DAY(TRUNC(dat,'IW'),'Domingo') TO_DATE into dataFim from dual;
return dataFim;
end;
/

--Procedimento que insere na tabela ResumoVeiculos a informa�ao da semana, da data recebida por parametro, das viagens dos veiculos
create or replace procedure procGuardarInformacaoSemanal(instante Timestamp) as
    data_inicio Date;
    data_fim Date;
    matricula veiculos.matricula%type;
    nr_viagens int;
    soma_km int;
    soma_duracao int;
    c SYS_REFCURSOR := funcObterInfoSemanalVeiculos(instante);
    numVeiculosTotal int;
    numVeiculosComViagens int := 0;
begin
    delete from ResumosVeiculos;
    select count(*) into numVeiculosTotal  from veiculos;
    begin
        loop
            fetch c into matricula, data_inicio, data_fim, nr_viagens, soma_km, soma_duracao;
            exit when c%notfound;
            if(nr_viagens>0) then
                numVeiculosComViagens := numVeiculosComViagens + 1;
                dbms_output.put_line(matricula);
                INSERT INTO ResumosVeiculos(instante, data_inicio, data_fim, matricula, nr_viagens, soma_km, soma_duracao)
                VALUES (instante, data_inicio, data_fim, matricula, nr_viagens, soma_km, soma_duracao);
            end if;
        end loop;
        close c;
        if(numVeiculosComViagens>0) then
            dbms_output.put_line('A percentagem de veiculos que efetuaram viagens nessa semana �: '|| (numVeiculosComViagens/numVeiculosTotal)*100 || '%');
        else 
            dbms_output.put_line('Nenhum veiculo efetuou viagens nessa semana logo a percentagem �: 0%');
        end if;
    end;
end;
/

--Blocos an�nimos
set serveroutput on;
declare
begin
    procGuardarInformacaoSemanal(TO_TIMESTAMP('2019-10-05 13:45', 'YYYY-MM-DD HH24:MI'));
    -- resultado esperado: o veiculo 11-AL-45 efetuou viagens nessa semana, percentagem: 20% pois existem 5 veiculos
end;
/

--Blocos an�nimos
set serveroutput on;
declare
begin
    procGuardarInformacaoSemanal(TO_TIMESTAMP('2019-09-13 20:48', 'yyyy-mm-dd hh24:mi'));
    -- resultado esperado: os veiculos 11-SD-12, 13-SF-99 efetuaram viagens nessa semana, percentagem: 40% pois existem 5 veiculos
end;
/

--Blocos an�nimos
set serveroutput on;
declare
begin
    procGuardarInformacaoSemanal(TO_TIMESTAMP('1980-03-13 10:48', 'yyyy-mm-dd hh24:mi'));
    -- resultado esperado: nenhum veiculo efetuou viagens nessa semana.
end;
/
