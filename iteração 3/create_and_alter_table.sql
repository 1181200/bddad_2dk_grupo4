-- ** eliminar tabelas se existentes **
-- CASCADE CONSTRAINTS para eliminar as restri��es de integridade das chaves prim�rias e chaves �nicas
-- PURGE elimina a tabela da base de dados e da "reciclagem"
DROP TABLE veiculos       CASCADE CONSTRAINTS PURGE;
DROP TABLE veiculos_condutores  CASCADE CONSTRAINTS PURGE;
DROP TABLE condutores CASCADE CONSTRAINTS PURGE;
DROP TABLE viagens     CASCADE CONSTRAINTS PURGE;
DROP TABLE pedidos_viagens  CASCADE CONSTRAINTS PURGE;
DROP TABLE servicos CASCADE CONSTRAINTS PURGE;
DROP TABLE itinerarios_viagens       CASCADE CONSTRAINTS PURGE;
DROP TABLE pontos_turisticos  CASCADE CONSTRAINTS PURGE;
DROP TABLE custos_servicos CASCADE CONSTRAINTS PURGE;
DROP TABLE clientes CASCADE CONSTRAINTS PURGE;
DROP TABLE recibos_viagens CASCADE CONSTRAINTS PURGE;
DROP TABLE recibos_condutores CASCADE CONSTRAINTS PURGE;


-- ## tabela veiculos ##
CREATE TABLE veiculos (
    matricula               CHAR(8)         CONSTRAINT pk_veiculos_matricula PRIMARY KEY,
    marca                   VARCHAR(40)     CONSTRAINT nn_veiculos_marca NOT NULL,
    modelo                  VARCHAR(40)     CONSTRAINT nn_veiculos_modelo NOT NULL,
    nr_chassis              VARCHAR(17)     CONSTRAINT uk_veiculos_nr_chassis UNIQUE,
                                            --CONSTRAINT ck_veiculos_nr_chassis CHECK (REGEXP_LIKE(nr_chassis, '((?=.*[^IOQ])(?=.[A-Z])(?=.\d)){17}')),
    data_matricula          DATE            CONSTRAINT nn_veiculos_data_matricula NOT NULL,
    km_disponiveis          INTEGER         CONSTRAINT nn_veiculos_km_disponiveis NOT NULL
    
    CONSTRAINT ck_veiculos_matricula CHECK (REGEXP_LIKE(matricula, '^\d{2}-[A-Z]{2}-\d{2}$|^\d{2}-\d{2}-[A-Z]{2}$|^[A-Z]{2}-\d{2}-\d{2}$'))    
);

-- ## tabela veiculos_condutores ##
CREATE TABLE veiculos_condutores (
    matricula               CHAR(8),
    nr_idCivil              INTEGER,
    data_inicio             DATE,
    data_fim                DATE            CONSTRAINT nn_veiculos_condutores_data_fim NOT NULL,
    
    CONSTRAINT pk_veiculos_condutores_matricula_nr_idCivil_data_inicio PRIMARY KEY (matricula, nr_idCivil, data_inicio)
);

-- ## tabela condutores ##
CREATE TABLE condutores (
    nr_idCivil              INTEGER         CONSTRAINT pk_condutores_nr_idCivil PRIMARY KEY
                                            CONSTRAINT ck_clientes_nr_idCivil          CHECK(REGEXP_LIKE(nr_idCivil, '^\d{9}$')),
    nr_idCivil_supervisor   INTEGER,
    nome                    VARCHAR(40)     CONSTRAINT nn_condutores_nome NOT NULL,
    morada                  VARCHAR(40)     CONSTRAINT nn_condutores_morada NOT NULL,
    data_nascimento         DATE            CONSTRAINT nn_condutores_data_nascimento NOT NULL,
    nr_carta_conducao       CHAR(11)        CONSTRAINT nn_condutores_nr_carta_conducao NOT NULL
                                            CONSTRAINT uk_condutores_nr_carta_conducao UNIQUE,
    data_validade_carta_conducao    DATE    CONSTRAINT nn_condutores_data_validade_carta_conducao NOT NULL,
    
    CONSTRAINT ck_condutores_nr_carta_conducao CHECK (REGEXP_LIKE(nr_carta_conducao, '^[A-Z]{2}-[0-9]{6}\s[0-9]{1}'))

);

-- ## tabela pedidos_viagens ## --
CREATE TABLE pedidos_viagens (
    cod_pedido              INTEGER GENERATED AS IDENTITY CONSTRAINT pk_pedidos_viagens_cod_pedido PRIMARY KEY,
    matricula               CHAR(8),
    nr_idCivil              INTEGER,
    login                   VARCHAR(15)                   CONSTRAINT nn_pedidos_viagens_login NOT NULL,
    data_inicio             DATE,
    cod_servico             INTEGER,
    data_hora_pedido        TIMESTAMP                     CONSTRAINT nn_pedidos_viagens_data_hora_pedido NOT NULL,
    data_hora_recolha_passageiro    TIMESTAMP             CONSTRAINT nn_pedidos_viagens_data_hora_recolha_passageiro NOT NULL,
    distancia_km            INTEGER                       CONSTRAINT nn_pedidos_viagens_distancia_km NOT NULL
                                                          CONSTRAINT ck_pedidos_viagens_distancia_km CHECK (distancia_km > 0),
    data_hora_cancelamento  TIMESTAMP                     
    
    
    
    CONSTRAINT ck_data_hora_pedido CHECK (data_hora_pedido < data_hora_recolha_passageiro)
    CONSTRAINT data_hora_cancelamento CHECK (data_hora_cancelamento < data_hora_recolha_passageiro)
                                                          
);

-- ** alterar tabelas para defini��o de chaves estrangeiras **
-- ## tabela condutores ## --
ALTER TABLE condutores ADD CONSTRAINT fk_condutores_nr_idCivil_supervisor  FOREIGN KEY (nr_idCivil) REFERENCES condutores (nr_idCivil);

-- ## tabela veiculos_condutores ##
ALTER TABLE veiculos_condutores ADD CONSTRAINT fk_veiculos_matricula   FOREIGN KEY (matricula) REFERENCES veiculos (matricula);
ALTER TABLE veiculos_condutores ADD CONSTRAINT fk_veiculos_condutores_nr_idCivil  FOREIGN KEY (nr_idCivil) REFERENCES condutores (nr_idCivil);

-- ## tabela pedidos_viagens ## --
ALTER TABLE pedidos_viagens ADD CONSTRAINT fk_pedidos_viagens_matricula_nr_idCivil_data_inicio FOREIGN KEY (matricula, nr_idCivil, data_inicio) REFERENCES veiculos_condutores (matricula, nr_idCivil, data_inicio);
ALTER TABLE pedidos_viagens ADD CONSTRAINT fk_pedidos_viagens_cod_servico  FOREIGN KEY (cod_servico) REFERENCES servicos (cod_servico);
ALTER TABLE pedidos_viagens ADD CONSTRAINT fk_clientes_login  FOREIGN KEY (login) REFERENCES clientes (login);

--Criar tabela servi�os 

create table servicos (
    cod_servico INTEGER CONSTRAINT pk_servicos_cod_servico PRIMARY KEY,
    descricao VARCHAR(50) CONSTRAINT nn_servicos_descricao NOT NULL
    );

--Criar tabela custos_servicos

create table custos_servicos (
    cod_servico INTEGER,
    data_inicio_custo TIMESTAMP CONSTRAINT nn_custos_servicos_data_inicio_custo NOT NULL,
    data_fim_custo TIMESTAMP,
    preco_base DECIMAL CONSTRAINT nn_custos_servicos_preco_base NOT NULL,
    custo_minuto DECIMAL CONSTRAINT nn_custos_servicos_custo_minuto NOT NULL,
    custo_km DECIMAL CONSTRAINT nn_custos_servicos_custo_km NOT NULL,
    taxa_IVA DECIMAL CONSTRAINT nn_custos_servicos_taxa_IVA NOT NULL,
    tempo_maximo_espera_minutos INTEGER CONSTRAINT nn_custos_servicos_tempo_maximo_espera_minutos NOT NULL,
    custo_espera_minuto DECIMAL CONSTRAINT nn_custos_servicos_custo_espera_minuto NOT NULL,
    custo_cancelamento_pedido DECIMAL CONSTRAINT nn_custos_servicos_custo_cancelamento_pedido NOT NULL,
    percentagem_cancelamento_tardio DECIMAL CONSTRAINT nn_custos_servicos_percentagem_cancelamento_tardio NOT NULL,
    percentagem_supervisor DECIMAL CONSTRAINT nn_custos_servicos_percentagem_supervisor NOT NULL,
    percentagem_viagem DECIMAL CONSTRAINT nn_custos_servicos_percentagem_viagem NOT NULL,
    
    CONSTRAINT pk_custos_servicos_cod_servico_data_inicio_custo PRIMARY KEY(cod_servico, data_inicio_custo)
    );
    
--Altera��o da tabela para inser��o de chave estrangeira(cod_cd)    
ALTER TABLE custos_servicos ADD CONSTRAINT fk_custos_servicos_cod_servico FOREIGN KEY(cod_servico) REFERENCES servicos(cod_servico);

--Cria��o da tabela pontos_turisticos

create table pontos_turisticos (
    cod_ponto_turistico INTEGER CONSTRAINT pk_pontos_turisticos_cod_ponto_turistico PRIMARY KEY,
    nome_ponto_turistico VARCHAR(20) CONSTRAINT nn_pontos_turisticos_nome_ponto_turistico NOT NULL,
    tipo_ponto_turistico VARCHAR(2) CONSTRAINT ck_pontos_turisticos_tipo_ponto_turistico CHECK(tipo_ponto_turistico = 'M' OR tipo_ponto_turistico = 'MU' OR tipo_ponto_turistico = 'PN' OR tipo_ponto_turistico = 'MI')
    );

create table viagens (
    cod_viagem INTEGER CONSTRAINT pk_viagens_cod_viagem PRIMARY KEY,
    cod_pedido INTEGER CONSTRAINT nn_viagens_cod_pedido NOT NULL,
    atraso_passageiro_minutos INTEGER CONSTRAINT nn_viagens_atraso_passageiro_minutos NOT NULL
                                      CONSTRAINT ck_viagens_atraso_passageiro_minutos CHECK(atraso_passageiro_minutos >= 0),
    local_inicio VARCHAR(40)          CONSTRAINT nn_viagens_local_inicio NOT NULL,
    duracao_minutos INTEGER CONSTRAINT nn_viagens_duracao_minutos NOT NULL
                            CONSTRAINT ck_viagens_duracao_minutos CHECK(duracao_minutos >= 0)
    );
    
ALTER TABLE viagens ADD CONSTRAINT cod_pedido FOREIGN KEY(cod_pedido) REFERENCES pedidos_viagens(cod_pedido);

create table itinerarios_viagens (
    cod_viagem INTEGER CONSTRAINT nn_itinerarios_viagens_cod_viagem NOT NULL,
    cod_ponto_turistico INTEGER CONSTRAINT nn_itinerarios_viagens_cod_ponto_turistico NOT NULL, 
    hora_passagem TIMESTAMP CONSTRAINT nn_itinerarios_viagens_hora_passagem NOT NULL,
    
    CONSTRAINT pk_itinerarios_viagens_cod_viagem_cod_ponto_turistico PRIMARY KEY(cod_viagem, cod_ponto_turistico)
    );
    
create table clientes (
    login  VARCHAR(10) CONSTRAINT pk_clientes_login PRIMARY KEY,
    password VARCHAR(20) CONSTRAINT nn_clientes_password NOT NULL,
    nr_id_civil INTEGER CONSTRAINT nn_clientes_nr_id_civil NOT NULL
                        CONSTRAINT uk_clientes_nr_id_civil UNIQUE,
    nome VARCHAR(40) CONSTRAINT nn_clientes_nome NOT NULL,
    nr_contribuinte INTEGER CONSTRAINT nn_clientes_nr_contribuinte NOT NULL
                            CONSTRAINT uk_clientes_nr_contribuinte UNIQUE,
    data_nascimento TIMESTAMP CONSTRAINT nn_clientes_data_nascimento NOT NULL
);
    
ALTER TABLE itinerarios_viagens ADD CONSTRAINT fk_itinerarios_viagens_cod_viagem FOREIGN KEY(cod_viagem) REFERENCES viagens(cod_viagem);
ALTER TABLE itinerarios_viagens ADD CONSTRAINT fk_itinerarios_viagens_cod_ponto_turistico FOREIGN KEY(cod_ponto_turistico) REFERENCES pontos_turisticos(cod_ponto_turistico);

create table recibos_viagens (
    nr_recibo INTEGER GENERATED AS IDENTITY CONSTRAINT pk_recibos_viagens_nr_recibo PRIMARY KEY,
    nr_viagem INTEGER,
    cod_servico INTEGER,
    custo_total DECIMAL CONSTRAINT nn_recibos_viagens_custo_total NOT NULL,
    percentagem_viagem DECIMAL CONSTRAINT nn_recibos_viagens_percentagem_viagem NOT NULL,
    valor_comissao DECIMAL CONSTRAINT nn_recibos_viagens_valor_comissao NOT NULL,
    nr_id_civil_condutor INTEGER
);

ALTER TABLE recibos_viagens ADD CONSTRAINT fk_recibos_viagens_nr_viagem FOREIGN KEY (nr_viagem) REFERENCES viagens(cod_viagem);
ALTER TABLE recibos_viagens ADD CONSTRAINT fk_recibos_viagens_cod_servico FOREIGN KEY (cod_servico) REFERENCES viagens(cod_servico);
ALTER TABLE recibos_viagens ADD CONSTRAINT fk_recibos_viagens_nr_id_civil_condutor FOREIGN KEY (nr_id_civil_condutor) REFERENCES condutores(nr_idCivil);

create table recibos_condutores (
    nr_recibo INTEGER GENERATED AS IDENTITY CONSTRAINT pk_recibos_condutores_nr_recibo PRIMARY KEY,
    nr_id_civil_condutor INTEGER CONSTRAINT nn_nr_id_civil_condutor NOT NULL,
    data_emissao TIMESTAMP CONSTRAINT nn_recibos_condutores_data_emissao NOT NULL,
    valor_total_comissoes DECIMAL CONSTRAINT nn_recibos_condutores_valor_total_comissoes NOT NULL
);

ALTER TABLE recibos_condutores ADD CONSTRAINT fk_recibos_condutores_nr_id_civil_condutor FOREIGN KEY (nr_id_civil_condutor) REFERENCES condutores(nr_idCivil);


    
    




