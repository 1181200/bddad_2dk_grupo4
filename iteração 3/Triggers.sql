set serveroutput on;

CREATE OR REPLACE TRIGGER trgAtribuicaoPedido
BEFORE INSERT ON pedidos_viagens

for each row
declare
counter number;
ex_sem_condutores_alocados exception;
ex_sem_condutores_disponiveis exception;
ex_pedido_cancelado exception;
idcivil veiculos_condutores.nr_Idcivil%type;
matr veiculos_condutores.matricula%type;
dataI veiculos_condutores.data_inicio%type;
dataF veiculos_condutores.data_fim%type;
kms veiculos.km_disponiveis%type;


cursor condutores_disponiveis is 
        SELECT vc.matricula, vc.nr_idcivil, vc.data_inicio, vc.data_fim, v.km_disponiveis
        FROM veiculos_condutores vc
        INNER JOIN veiculos v on v.matricula = vc.matricula;
        
BEGIN 

if :new.data_hora_cancelamento is not null then
    raise ex_pedido_cancelado;
end if;

SELECT count(*) INTO counter
FROM veiculos_condutores;

if counter = 0 THEN
    raise ex_sem_condutores_alocados;
end if;
    

SELECT count(*) INTO counter
FROM veiculos_condutores vc
WHERE :new.data_hora_recolha_passageiro > vc.data_inicio and :new.data_hora_recolha_passageiro < vc.data_fim;

if counter = 0 THEN
    raise ex_sem_condutores_disponiveis;
end if;

open condutores_disponiveis;
loop
    fetch condutores_disponiveis into matr, idCivil, dataI, dataF, kms;
    exit when condutores_disponiveis%notfound or :new.matricula is not null;
    
    if :new.data_hora_recolha_passageiro > dataI AND :new.data_hora_recolha_passageiro < dataF then
        select count(*) INTO counter
            from (
                select distinct pv.*, vei.km_disponiveis
                from pedidos_viagens pv
                inner join viagens v on v.cod_pedido = pv.cod_pedido
                inner join veiculos_condutores vc on vc.matricula = pv.matricula and vc.nr_idcivil = pv.nr_idcivil and vc.data_inicio = pv.data_inicio
                inner join veiculos vei on vei.matricula = pv.matricula
                where vc.matricula = matr and vc.nr_idCivil = idCivil and vc.data_inicio = dataI
                and (vei.km_disponiveis - :new.distancia_km < 0 or
                pv.data_hora_recolha_passageiro + NUMTODSINTERVAL(v.duracao_minutos, 'MINUTE') > :new.data_hora_recolha_passageiro));
            
        
    if counter = 0 then
        dbms_output.put_line('Pedido atualizado!!');
        :new.matricula := matr;
        :new.nr_idCivil := idcivil;
        :new.data_inicio := dataI;
    end if;
    end if;
end loop;
    
    if :new.nr_idCivil is null then
        raise ex_sem_condutores_disponiveis;
    end if;


EXCEPTION
    WHEN ex_sem_condutores_alocados then
        raise_application_error(-20101, 'N�o existem condutores com ve�culos alocados');
    WHEN ex_sem_condutores_disponiveis then
        raise_application_error(-20001, 'N�o existem condutores dispon�veis para a data e hora pedida');
    WHEN ex_pedido_cancelado then
        raise_application_error(-20002, 'O pedido foi cancelado, n�o � necess�rio alocar condutor');
END trgAtribuicaoPedido; 





set serveroutput on;
BEGIN
INSERT INTO pedidos_viagens(matricula, nr_idCivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, data_hora_cancelamento, login)
VALUES(null, null, null, '1', TO_TIMESTAMP('2019-08-29 12:11', 'yyyy-mm-dd hh24-mi'),
TO_TIMESTAMP('2019-08-29 12:21', 'YYYY-MM-DD HH24-MI'), 6, TO_TIMESTAMP('2019-08-29 10:21', 'YYYY-MM-DD HH24-MI'),'castico');
end;

set serveroutput on;
BEGIN
INSERT INTO pedidos_viagens(matricula, nr_idCivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, data_hora_cancelamento, login)
    VALUES(null, null, null, '2', TO_TIMESTAMP('2019-09-13 20:45', 'yyyy-mm-dd hh24:mi'), TO_TIMESTAMP('2019-11-20 20:48', 'yyyy-mm-dd hh24:mi'), 9, null,'homemorcego');
end;

CREATE OR REPLACE TRIGGER trgCondutoresVeiculosAssociacoes
BEFORE INSERT OR UPDATE ON associacoes_veiculo_condutor FOR EACH ROW
    DECLARE
        sobreposicao_temporal_encontrada EXCEPTION;
        associacoes_matricula CURSOR;
    BEGIN
        --Procura todas as associa��es com o condutor ou o v+eiculo pretendidos
        FOR associacoes_matricula IN (SELECT AVC.nr_associacao, AVC.data_inicio di, NVL(AVC.data_fim, SYSDATE) df
                                        FROM associacoes_veiculo_condutor AVC 
                                        WHERE AVC.matricula = :NEW.matricula OR AVC.nr_id_civil = :NEW.nr_id_civil)
        
        --Para cada uma das associa��es, verificar se existe uma sobreposi��o temporal
        LOOP
            IF NOT (:NEW.data_inicio > associacoes_matricula.df AND :NEW.data_fim > associacoes_matricula.df) 
                OR (:NEW.data_inicio < associacoes_matricula.di AND :NEW.data_fim < associacoes_matricula.di) THEN
                RAISE sobreposicao_temporal_encontrada;
            END IF;
        END LOOP;
        
        --Tratamento de exce��es
        EXCEPTION
            WHEN sobreposicao_temporal_encontrada THEN
                RAISE_APPLICATION_ERROR(-20020, 'Foi encontrada uma sobreposi��o temporal.');
    END;
/

--Exerc�cio 3:

CREATE OR REPLACE TRIGGER trgCondutoresImpedirSupervisores
BEFORE INSERT OR UPDATE ON condutores FOR EACH ROW
    DECLARE
        supervisor_nao_registado EXCEPTION;
        supervisor_invalido EXCEPTION;
        supervisor_carta_conducao_invalida EXCEPTION;
        supervisor_menos_viagens EXCEPTION;
        supervisor_menos_idade EXCEPTION;
        a condutores%ROWTYPE;
        nr_viagens_realizadas_supervisor INTEGER;
        nr_viagens_realizadas_condutor INTEGER;
        idade_supervisor INTEGER;
        idade_condutor INTEGER;
    BEGIN
        --Primeiro, verifica-se se o supervisor � v�lido.
        --Este � v�lido se estiver registado na base de dados e o nr_id_civil_supervisor for nulo.
        SELECT * INTO a FROM condutores C WHERE :NEW.nr_id_civil_supervisor = C.nr_id_civil_supervisor;
        IF a IS NULL THEN
            RAISE supervisor_nao_registado;
        END IF;
        IF a.nr_id_civil_supervisor IS NOT NULL THEN
            RAISE supervisor_invalido;
        END IF;
        
        --Se a carta de condu��o n�o foi obtida � mais tempo
        IF a.data_obtencao_carta_conducao <= :NEW.data_obtencao_carta_conducao THEN
            RAISE supervisor_carta_conducao_invalida;
        END IF;
        
        --Calcula o n�mero de viagens do supervisor
        SELECT COUNT(*) INTO nr_viagens_realizadas_supervisor 
            FROM pedidos_viagens PV 
                INNER JOIN associacoes_veiculo_condutor AVC ON AVC.nr_associacao = PV.nr_associacao
            WHERE AVC.nr_id_civil = a.nr_id_civil;
        --Calcula o n�mero de viagens do supervisionado
        SELECT COUNT(*) INTO nr_viagens_realizadas_condutor 
            FROM pedidos_viagens PV 
                INNER JOIN associacoes_veiculo_condutor AVC ON AVC.nr_associacao = PV.nr_associacao
            WHERE AVC.nr_id_civil = :NEW.nr_id_civil;
            
        --Se o n�mero de viagens do supervisor for menor que as do supervisionado, lan�ar uma exce��o.
        IF nr_viagens_realizadas_supervisor <= nr_viagens_realizadas_condutor THEN
            RAISE supervisor_menos_viagens;
        END IF;
        
        --Calcula as idades do supervisor e supervisionado
        idade_supervisor := TRUNC(MONTHS_BETWEEN(SYSDATE, a.data_nascimento)/12);
        idade_condutor := TRUNC(MONTHS_BETWEEN(SYSDATE, :NEW.data_nascimento)/12);
            
        --Se a idade do supervisor for menor que a do supervisionado mais cinco, lan�ar uma exce��o.
        IF idade_supervisor + 5 <= idade_condutor THEN
            RAISE supervisor_menos_idade;
        END IF;
        
        EXCEPTION
            WHEN supervisor_nao_registado THEN
                --Verificar se a opera��o DML � um INSERT ou um DELETE
                IF :OLD.nr_id_civil IS NULL THEN
                    RAISE_APPLICATION_ERROR(-20012, 'N�o foi poss�vel efetuar o INSERT pois o supervisor n�o est� registado.');
                ELSE
                    RAISE_APPLICATION_ERROR(-20013, 'N�o foi poss�vel efetuar o UPDATE pois o supervisor n�o est� registado.');
                END IF;
            WHEN supervisor_invalido THEN
                --Verificar se a opera��o DML � um INSERT ou um DELETE
                IF :OLD.nr_id_civil IS NULL THEN
                    RAISE_APPLICATION_ERROR(-20014, 'N�o foi poss�vel efetuar o INSERT pois o id civil n�o corresponde a um supervisor.');
                ELSE
                    RAISE_APPLICATION_ERROR(-20015, 'N�o foi poss�vel efetuar o UPDATE pois o id civil n�o corresponde a um supervisor.');
                END IF;
            WHEN supervisor_carta_conducao_invalida THEN
                --Verificar se a opera��o DML � um INSERT ou um DELETE
                IF :OLD.nr_id_civil IS NULL THEN
                    RAISE_APPLICATION_ERROR(-20014, 'N�o foi poss�vel efetuar o INSERT pois o supervisor obteve carta de condu��o h� menor tempo.');
                ELSE
                    RAISE_APPLICATION_ERROR(-20015, 'N�o foi poss�vel efetuar o UPDATE pois o supervisor obteve carta de condu��o h� menor tempo.');
                END IF;
            WHEN supervisor_menos_viagens THEN
                --Verificar se a opera��o DML � um INSERT ou um DELETE
                IF :OLD.nr_id_civil IS NULL THEN
                    RAISE_APPLICATION_ERROR(-20016, 'N�o foi poss�vel efetuar o INSERT pois o supervisor efetuou menos viagens que o supervisionado.');
                ELSE
                    RAISE_APPLICATION_ERROR(-20017, 'N�o foi poss�vel efetuar o UPDATE pois o supervisor efetuou menos viagens que o supervisionado.');
                END IF;
            WHEN supervisor_menos_idade THEN
                --Verificar se a opera��o DML � um INSERT ou um DELETE
                IF :OLD.nr_id_civil IS NULL THEN
                    RAISE_APPLICATION_ERROR(-20018, 'N�o foi poss�vel efetuar o INSERT pois o supervisor n�o � mais velho que o supervisionado por cinco anos.');
                ELSE
                    RAISE_APPLICATION_ERROR(-20019, 'N�o foi poss�vel efetuar o UPDATE pois o supervisor n�o � mais velho que o supervisionado por cinco anos.');
                END IF;
        END;
/

