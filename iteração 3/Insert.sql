-- ** inserir dados nas tabelas **

-- ## tabela Veiculos ##
INSERT INTO Veiculos(matricula, marca, modelo, nr_chassis, data_matricula, km_disponiveis)
     VALUES ('11-AZ-23', 'Audi', 'A1', '9BGRD08X04G117974', TO_DATE('2005-11-14', 'yyyy-mm-dd'), '2000');
INSERT INTO Veiculos(matricula, marca, modelo, nr_chassis, data_matricula, km_disponiveis)
     VALUES ('11-SD-12', 'Toyota', 'Yaris', '9BGRD08X34G3S7974', TO_DATE('2012-10-02', 'yyyy-mm-dd'), '1500');
INSERT INTO Veiculos(matricula, marca, modelo, nr_chassis, data_matricula, km_disponiveis)
     VALUES ('13-SF-99', 'Toyota', 'Corolla', '9BPTD28F04G115974', TO_DATE('2018-12-23', 'yyyy-mm-dd'), '1700');
INSERT INTO Veiculos(matricula, marca, modelo, nr_chassis, data_matricula, km_disponiveis)
     VALUES ('11-45-QL', 'Opel', 'Corsa', '9BGRD08X04G123461', TO_DATE('2000-11-01', 'yyyy-mm-dd'), '500');
INSERT INTO Veiculos(matricula, marca, modelo, nr_chassis, data_matricula, km_disponiveis)
     VALUES ('11-AL-45', 'Bugatti', 'Chiron', '9BGRD08X04G234534', TO_DATE('2014-11-14', 'yyyy-mm-dd'), '350');
     
-- ## tabela Veiculos Condutores ##
INSERT INTO Veiculos_Condutores(matricula, nr_idCivil, data_inicio, data_fim)
    VALUES('11-AZ-23', '111111111', TO_DATE('2019-11-14', 'yyyy-mm-dd'), TO_DATE('2019-12-01', 'yyyy-mm-dd'));
INSERT INTO Veiculos_Condutores(matricula, nr_idCivil, data_inicio, data_fim)
    VALUES('11-SD-12', '222222222', TO_DATE('2019-12-14', 'yyyy-mm-dd'), TO_DATE('2020-02-01', 'yyyy-mm-dd'));
INSERT INTO Veiculos_Condutores(matricula, nr_idCivil, data_inicio, data_fim)
    VALUES('13-SF-99', '333333333', TO_DATE('2018-12-27', 'yyyy-mm-dd'), TO_DATE('2019-01-01', 'yyyy-mm-dd'));
INSERT INTO Veiculos_Condutores(matricula, nr_idCivil, data_inicio, data_fim)
    VALUES('11-45-QL', '444444444', TO_DATE('2019-12-12', 'yyyy-mm-dd'), TO_DATE('2019-12-31', 'yyyy-mm-dd'));
INSERT INTO Veiculos_Condutores(matricula, nr_idCivil, data_inicio, data_fim)
    VALUES('11-AL-45', '555555555', TO_DATE('2020-01-05', 'yyyy-mm-dd'), TO_DATE('2020-03-31', 'yyyy-mm-dd'));
    -- inserir mais para testar sobreposi��es
    
-- ## tabela Condutores ##
INSERT INTO Condutores(nr_idCivil, nr_idCivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao, morada)
    VALUES('111111111', '333333333', 'Alexandre Leite', TO_DATE('1980-01-23', 'yyyy-mm-dd'), 'AV-234543 7',TO_DATE('2025-03-24', 'yyyy-mm-dd'), 'porto');
INSERT INTO Condutores(nr_idCivil, nr_idCivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao, morada)
    VALUES('222222222', '111111111', 'Jos� Guedes', TO_DATE('1987-04-21', 'yyyy-mm-dd'), 'AV-123123 5',TO_DATE('2022-04-21', 'yyyy-mm-dd'), 'lisboa');
INSERT INTO Condutores(nr_idCivil, nr_idCivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao, morada)
    VALUES('333333333', NULL, 'Albertino Mendes', TO_DATE('1966-01-30', 'yyyy-mm-dd'), 'AV-854567 7',TO_DATE('2021-01-12', 'yyyy-mm-dd'), 'faro');
INSERT INTO Condutores(nr_idCivil, nr_idCivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao, morada)
    VALUES('444444444', '111111111', 'Joana Lopes', TO_DATE('1992-10-12', 'yyyy-mm-dd'), 'AV-179546 2',TO_DATE('2024-01-25', 'yyyy-mm-dd'), 'carcavelos');
INSERT INTO Condutores(nr_idCivil, nr_idCivil_supervisor, nome, data_nascimento, nr_carta_conducao, data_validade_carta_conducao, morada)
    VALUES('555555555', null, 'Alberto Reis', TO_DATE('1980-01-23', 'yyyy-mm-dd'), 'AV-134253 7',TO_DATE('2025-03-24', 'yyyy-mm-dd'), 'margon�a');

--## tabela Clientes ##
INSERT INTO Clientes(login, passwrd, nr_id_civil, nome, nr_contribuinte, data_nascimento)
    VALUES('arqueiroverde', 'qwerty', '123123123','Oliver Torres', '223123123', TO_DATE('1980-01-25', 'yyyy-mm-dd'));
INSERT INTO Clientes(login, passwrd, nr_id_civil, nome, nr_contribuinte, data_nascimento)
    VALUES('homemorcego', 'azerty', '666666666','Cristiano Nolino', '123456789', TO_DATE('1999-02-01', 'yyyy-mm-dd'));
INSERT INTO Clientes(login, passwrd, nr_id_civil, nome, nr_contribuinte, data_nascimento)
    VALUES('candido', 'password','777777777','C�ndido Oliveira', '987654321', TO_DATE('1985-01-01', 'yyyy-mm-dd'));
INSERT INTO Clientes(login, passwrd, nr_id_civil, nome, nr_contribuinte, data_nascimento)
    VALUES('bitwise', 'qwedw', '888888888', 'Assembly C', '111222333', TO_DATE('1945-09-25', 'yyyy-mm-dd'));
INSERT INTO Clientes(login, passwrd, nr_id_civil, nome, nr_contribuinte, data_nascimento)
    VALUES('castico', 'marcas', '999999999', 'Anselmo Enfadonho', '987987987', TO_DATE('1981-01-25', 'yyyy-mm-dd'));



-- ## tabela Viagens ##
INSERT INTO viagens(cod_viagem, cod_pedido, atraso_passageiro_minutos, local_inicio, duracao_minutos)
    VALUES('0097', '7', '2', 'rua dos combatentes', '20');
INSERT INTO viagens(cod_viagem, cod_pedido, atraso_passageiro_minutos, local_inicio, duracao_minutos)
    VALUES('0099', '8', '3', 'rua gon�alo mendes da maia', '25');
INSERT INTO viagens(cod_viagem, cod_pedido, atraso_passageiro_minutos, local_inicio, duracao_minutos)
    VALUES('0103', '9', '0', 'rua sara afonso', '16');
INSERT INTO viagens(cod_viagem, cod_pedido, atraso_passageiro_minutos, local_inicio, duracao_minutos)
    VALUES('0105', '10', '10', 'travessa da arrechousa','12');
INSERT INTO viagens(cod_viagem, cod_pedido, atraso_passageiro_minutos, local_inicio, duracao_minutos)
    VALUES('0095', '11', '5', 'malafaia', '10');
    
-- ## tabela Pedidos_Viagens ##
INSERT INTO pedidos_viagens(matricula, nr_idCivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, data_hora_cancelamento, login)
    VALUES(null, null, null, '3', TO_TIMESTAMP('2019-10-05 13:43', 'yyyy-mm-dd hh24:mi'), TO_TIMESTAMP('2019-10-05 13:45', 'YYYY-MM-DD HH24:MI'), 13, null,'arqueiroverde');
INSERT INTO pedidos_viagens(matricula, nr_idCivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, data_hora_cancelamento, login)
    VALUES(null, null, null, '2', TO_TIMESTAMP('2019-09-13 20:45', 'yyyy-mm-dd hh24:mi'), TO_TIMESTAMP('2019-09-13 20:48', 'yyyy-mm-dd hh24:mi'), 9, null,'homemorcego');
INSERT INTO pedidos_viagens(matricula, nr_idCivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, data_hora_cancelamento, login)
    VALUES(null, null, null, '2', TO_TIMESTAMP('2019-09-02 09:35', 'yyyy-mm-dd hh24-mi'), TO_TIMESTAMP('2019-09-13 09:35', 'yyyy-mm-dd hh24-mi'), 11, null,'candido');
INSERT INTO pedidos_viagens(matricula, nr_idCivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, data_hora_cancelamento, login)
    VALUES(null, null, null, '1', TO_TIMESTAMP('2019-08-29 12:11', 'yyyy-mm-dd hh24-mi'), TO_TIMESTAMP('2019-08-29 12:21', 'YYYY-MM-DD HH24-MI'), 6, TO_TIMESTAMP('2019-08-29 10:21', 'YYYY-MM-DD HH24-MI'),'castico');
INSERT INTO pedidos_viagens(matricula, nr_idCivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, data_hora_cancelamento, login)
    VALUES(null, null, null, '1', TO_TIMESTAMP('2019-11-22 17:48', 'yyyy-mm-dd hh24-mi'), TO_TIMESTAMP('2019-11-22 17:53', 'yyyy-mm-dd hh24-mi'), 5, null,'bitwise');
    
--## tabela Sevicos ##
INSERT INTO servicos(cod_servico, descricao)
    VALUES('1', 'Servi�o um');
INSERT INTO servicos(cod_servico, descricao)
    VALUES('2', 'Servi�o dois');
INSERT INTO servicos(cod_servico, descricao)
    VALUES('3', 'Servi�o tr�s');

--## tabela Pontos Turisticos ##
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000001,'Museu 1','M');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000002,'Museu 2','M');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000003,'Museu 3','M');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000004,'Museu 4','M');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000005,'Monumento 5','MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000006,'Monumento 6','MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000007,'Monumento 7','MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000008,'Monumento 8','MU');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico)    
    VALUES (00000009,'Parque Nacional 9','PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (000000010,'Parque Nacional 10','PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000011,'Parque Nacional 11','PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000012,'Parque Nacional 12','PN');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000013,'Miradouro 13','MI');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000014,'Miradouro 14','MI');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000015,'Miradouro 15','MI');
INSERT INTO pontos_turisticos(cod_ponto_turistico,nome_ponto_turistico,tipo_ponto_turistico) 
    VALUES (00000016,'Miradouro 16','MI');

--## tabela Custos Servi�os ##
INSERT INTO custos_servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, percentagem_cancelamento_tardio, percentagem_supervisor, percentagem_viagem) 
    VALUES('1',TO_DATE('2017-10-04','yyyy-mm-dd'), TO_DATE('2019-12-01','yyyy-mm-dd'), 105.2, 1.35, 0.75, 23.0, 30, 1.0, 55.1, 22.5, 20, 15);
INSERT INTO custos_servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, percentagem_cancelamento_tardio, percentagem_supervisor, percentagem_viagem) 
    VALUES('2',TO_DATE('2019-12-02','yyyy-mm-dd'), TO_DATE('2020-12-31','yyyy-mm-dd'), 120.2, 1.45, 1.50, 23.0, 25, 2.0, 60.1, 25, 15, 20);
INSERT INTO custos_servicos(cod_servico, data_inicio_custo, data_fim_custo, preco_base, custo_minuto, custo_km, taxa_IVA, tempo_maximo_espera_minutos, custo_espera_minuto, custo_cancelamento_pedido, percentagem_cancelamento_tardio, percentagem_supervisor, percentagem_viagem) 
    VALUES('3',TO_DATE('2021-01-01','yyyy-mm-dd'), TO_DATE('2021-02-01','yyyy-mm-dd'), 45.0, 1.85, 2, 23.0, 45, 3.0, 22.3, 30, 25, 50);

--## tabela Itinerarios Viagens ##
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (97,0002, TO_TIMESTAMP('2018-01-05 15:10', 'YYYY-MM-DD HH24:MI'));
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (99,0003, TO_TIMESTAMP('2018-01-05 15:11', 'YYYY-MM-DD HH24:MI'));
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (105,0004, TO_TIMESTAMP('2018-01-05 15:12', 'YYYY-MM-DD HH24:MI'));
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (103,0003, TO_TIMESTAMP('2018-01-05 15:13', 'YYYY-MM-DD HH24:MI'));
INSERT INTO itinerarios_viagens(cod_viagem,cod_ponto_turistico,hora_passagem) 
    VALUES (95,0005, TO_TIMESTAMP('2019-02-10 16:30', 'YYYY-MM-DD HH24:MI'));