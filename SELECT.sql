--a--
with condutores_e_veiculos as
(
select *
from condutores c
    INNER JOIN veiculos_condutores vc ON c.nr_idcivil = vc.nr_idcivil
    INNER JOIN veiculos v ON v.matricula = vc.matricula
)

select nome
from condutores_e_veiculos cev
    WHERE cev.marca = 'Toyota'
    GROUP BY nome
    HAVING count(nome) =(select MIN(count(nome))
                FROM condutores_e_veiculos cev
                WHERE cev.marca = 'Toyota'
                GROUP BY cev.nome);
--b--
-- Apresente por cada tipo de servi�o, os 3 condutores com os maiores valores de custos totais das viagens; --
with custos as
(
	select 
		S.COD_SERVICO
		,C.NR_IDCIVIL
		,((CS.PRECO_BASE + (CS.CUSTO_MINUTO * V.DURACAO_MINUTOS) + (CS.CUSTO_KM * PV.DISTANCIA_KM) + (CS.CUSTO_ESPERA_MINUTO * V.ATRASO_PASSAGEIRO_MINUTOS)) * CS.TAXA_IVA) CUSTO_VIAGEM
	from 
		PEDIDOS_VIAGENS PV
		left join VIAGENS V
			on PV.COD_PEDIDO = V.COD_PEDIDO
		left join SERVICOS S
			on S.COD_SERVICO = PV.COD_SERVICO
		left join CUSTOS_SERVICOS CS
			on CS.COD_SERVICO = S.COD_SERVICO
		left join VEICULOS_CONDUTORES VC
			on VC.MATRICULA = PV.MATRICULA
		left join CONDUTORES C
			on C.NR_IDCIVIL = VC.NR_IDCIVIL
	where
		CS.COD_SERVICO is not null 
		and ((CS.PRECO_BASE + (CS.CUSTO_MINUTO * V.DURACAO_MINUTOS) + (CS.CUSTO_KM * PV.DISTANCIA_KM) + (CS.CUSTO_ESPERA_MINUTO * V.ATRASO_PASSAGEIRO_MINUTOS)) * CS.TAXA_IVA) is not null
	group by
		S.COD_SERVICO
		,C.NR_IDCIVIL
		,((CS.PRECO_BASE + (CS.CUSTO_MINUTO * V.DURACAO_MINUTOS) + (CS.CUSTO_KM * PV.DISTANCIA_KM) + (CS.CUSTO_ESPERA_MINUTO * V.ATRASO_PASSAGEIRO_MINUTOS)) * CS.TAXA_IVA)
	order by
		NR_IDCIVIL desc
)

select 
	COD_SERVICO
	,NR_IDCIVIL
	,TOTAL
from 
(
	select distinct
        (ROW_NUMBER() OVER (PARTITION BY COD_SERVICO ORDER BY COD_SERVICO ASC)) as RN
		, COD_SERVICO
		,NR_IDCIVIL
		,SUM(CUSTO_VIAGEM) OVER (PARTITION BY COD_SERVICO, NR_IDCIVIL) TOTAL 
	from
		CUSTOS
	order by 
		cod_servico
		,nr_idcivil
		,TOTAL desc
) C
where 
	C.RN <= 3;
--c--
-- Apresente as viagens que possuem, no seu itiner�rio, somente pontos tur�sticos com a hora de
--passagem superior � m�dia da hora de passagem de todos os pontos tur�sticos do tipo �Museus�;

select vs.hora_passagem, vs.cod_viagem
from
(
    select
        (select avg(extract(hour from hora_passagem)) from itinerarios_viagens) media
        ,v.cod_viagem
        ,iv.hora_passagem
        ,pt.tipo_ponto_turistico
    from 
        pontos_turisticos pt
        inner join itinerarios_viagens iv 
            on pt.cod_ponto_turistico = iv.cod_ponto_turistico
        inner join viagens v 
            on v.cod_viagem = iv.cod_viagem
) vs
where
    vs.MEDIA < extract(hour from vs.hora_passagem)
    and vs.tipo_ponto_turistico = 'M';

--d--
--Apresente os dados dos pedidos de viagem a que foi alocado um ve�culo em que a �ltima viagem
--que o ve�culo fez teve uma hora de in�cio (data_hora_recolha_passageiro) superior a duas horas em
--rela��o ao tempo de chegada da viagem correspondente a esse pedido;

SELECT matricula, nr_idCivil, data_inicio, cod_servico, data_hora_pedido, data_hora_recolha_passageiro, distancia_km, cancelado
FROM pedidos_viagens
WHERE pedidos_viagens.matricula = (select veiculos_condutores.matricula 
                                   from veiculos_condutores inner join pedidos_viagens
                                   on veiculos_condutores.matricula = pedidos_viagens.matricula
                                   where ( select viagens.duracao_minutos
                                           from viagens left outer join pedidos_viagens
                                           on viagens.cod_pedido = pedidos_viagens.cod_pedido) > 2);

--e--
--Apresente a data, a hora, a matr�cula do ve�culo e o nome dos condutores que efetuaram viagens
--somente �s ter�as-feiras, quintas-feiras, s�bados e domingos, durante os �ltimos 6 meses, no
--per�odo das 10 horas �s 15 horas. No caso de algum condutor ter efetuado mais de 3 viagens aos
--s�bados e domingos, dever� apresentar o custo total dessas viagens numa coluna intitulada
--�Custo_Total_Viagens�. O resultado devera? ser apresentado por ordem alfabe?tica por nome de
--condutor e por ordem descendente da data e hora;

truncate table tmp_report_final;
drop table tmp_report_final;


create global temporary table tmp_custos_totais(cod_servico integer, custo_total numeric(8, 2)) on commit preserve rows;
create global temporary table tmp_custos_anuais(cod_servico integer, custo_anual numeric(8, 2)) on commit preserve rows;
create global temporary table tmp_medias(cod_servico integer, media numeric(8, 2)) on commit preserve rows;
create global temporary table tmp_custos_pedido(cod_servico integer, custo_pedido numeric(8, 2)) on commit preserve rows;
create global temporary table tmp_report_final(cod_servico integer, custo_total numeric(8, 2), relacao numeric(8, 2), num_pedidos integer) on commit preserve rows;

insert into tmp_custos_totais 
select pv.cod_servico, sum(cs.preco_base+(cs.custo_minuto*v.duracao_minutos)+(cs.custo_km*pv.distancia_km)+(cs.custo_espera_minuto*v.atraso_passageiro_minutos)+cs.taxa_iva) as custo_total
from pedidos_viagens pv
inner join custos_servicos cs on cs.cod_servico = pv.cod_servico
inner join viagens v on v.cod_pedido = pv.cod_pedido
group by pv.cod_servico;

insert into tmp_custos_anuais
select distinct pv.cod_servico, sum(cs.preco_base+(cs.custo_minuto*v.duracao_minutos)+(cs.custo_km*pv.distancia_km)+(cs.custo_espera_minuto*v.atraso_passageiro_minutos)+cs.taxa_iva) over (partition by pv.cod_servico, extract(year from pv.data_hora_pedido)) custo_anual
from pedidos_viagens pv
inner join custos_servicos cs on cs.cod_servico = pv.cod_servico
inner join viagens v on v.cod_pedido = pv.cod_pedido
where extract(year from pv.data_hora_pedido) = '2019';

insert into tmp_medias
select s.cod_servico, avg(cs.preco_base+(cs.custo_minuto*v.duracao_minutos)+(cs.custo_km*pv.distancia_km)+(cs.custo_espera_minuto*v.atraso_passageiro_minutos)+cs.taxa_iva) as media
from servicos s
inner join custos_servicos cs on s.cod_servico = cs.cod_servico
inner join pedidos_viagens pv on pv.cod_servico = s.cod_servico
inner join viagens v on v.cod_pedido = pv.cod_pedido
group by s.cod_servico;

insert into tmp_custos_pedido
select pv.cod_servico, (cs.preco_base + ( cs.custo_minuto * v.duracao_minutos ) + (cs.custo_km*pv.distancia_km)+(cs.custo_espera_minuto * v.atraso_passageiro_minutos)+cs.taxa_iva) custo_pedido
from pedidos_viagens pv
inner join custos_servicos cs on cs.cod_servico = pv.cod_servico
inner join tmp_medias tm on pv.cod_servico = tm.cod_servico
inner join viagens v on v.cod_pedido = pv.cod_pedido
where (cs.preco_base + ( cs.custo_minuto * v.duracao_minutos ) + (cs.custo_km*pv.distancia_km)+(cs.custo_espera_minuto * v.atraso_passageiro_minutos)+cs.taxa_iva) > (tm.media+(tm.media*0.80));

insert into tmp_report_final
select tct.cod_servico, tct.custo_total, (tca.custo_anual / tct.custo_total * 100) relacao, count(tcp.custo_pedido) num_pedidos
from tmp_custos_totais tct
left join tmp_custos_pedido tcp on tcp.cod_servico = tct.cod_servico
inner join tmp_custos_anuais tca on tca.cod_servico = tct.cod_servico
inner join tmp_medias tm on tca.cod_servico = tm.cod_servico
inner join custos_servicos cs on tca.cod_servico = tct.cod_servico
inner join pedidos_viagens pv on pv.cod_servico = tct.cod_servico
inner join viagens v on v.cod_pedido = pv.cod_pedido
group by tct.cod_servico, tct.custo_total, tca.custo_anual, tm.media
order by tct.cod_servico;

truncate table tmp_custos_totais; 
truncate table tmp_custos_anuais; 
truncate table tmp_medias;
truncate table tmp_custos_pedido;

drop table tmp_custos_totais;
drop table tmp_custos_anuais;
drop table tmp_medias;
drop table tmp_custos_pedido;

select * from tmp_report_final;

--f) Apresente para cada condutor, que n�o seja supervisor, o nome, o nr_idCivil, as viagens que realizou a um custo inferior � 
--    m�dia do custo das 20% melhores viagens do seu supervisor;

WITH aux AS (
    SELECT (CS.preco_base + CS.custo_minuto*v.duracao_minutos) + (CS.custo_km * PV.distancia_km)
            + (CS.custo_espera_minuto * V.atraso_passageiro_minutos) a, CS.cod_servico b, PV.cod_pedido c, PV.nr_idCivil d, PV.cancelado e 
    FROM custos_servicos CS
        INNER JOIN pedidos_viagens PV ON CS.cod_servico=PV.cod_servico 
        INNER JOIN viagens V ON PV.cod_pedido = V.cod_pedido),

custo_viagem AS (
    SELECT (aux.a + cs.taxa_iva * aux.a) custo_viagem, aux.c cod_pedido, aux.d nr_idCivil 
    FROM custos_servicos cs, aux 
    WHERE cs.cod_servico = aux.b AND aux.e = 'n'
    UNION
    SELECT cs.custo_cancelamento_pedido custo_viagem, aux.c cod_pedido, aux.d nr_idCivil 
    FROM custos_servicos cs, aux 
    WHERE cs.cod_servico = aux.b AND aux.e = 's')

SELECT C.nome, C.nr_idCivil, PV.cod_pedido
FROM condutores C 
    INNER JOIN pedidos_viagens PV ON C.nr_idCivil = PV.nr_idCivil 
    INNER JOIN custo_viagem CV ON CV.cod_pedido = PV.cod_pedido
WHERE C.nr_idCivil_supervisor IS NOT NULL AND CV.custo_viagem < (
                                                    SELECT AVG(
                                                        (SELECT CV.custo_viagem 
                                                            FROM custo_viagem CV 
                                                            WHERE CV.nr_idCivil = C.nr_idCivil_supervisor 
                                                            ORDER BY 1 DESC
                                                            FETCH FIRST 20 PERCENT ROWS ONLY)
                                                                ) FROM DUAL);
                                                                
--G--
with tabela as (
    select c.nome, pv.matricula, to_char(pv.data_hora_recolha_passageiro, 'yyyy-mm-dd') AS data, to_char(pv.Data_hora_recolha_passageiro, 'hh24:mi') AS hora, cs.preco_base+(cs.custo_minuto*v.duracao_minutos)+(cs.custo_km*pv.distancia_km)+(cs.custo_espera_minuto*v.atraso_passageiro_minutos)+cs.taxa_iva AS custo
    from pedidos_viagens pv
        INNER JOIN condutores c ON pv.nr_idcivil = c.nr_idcivil
        INNER JOIN viagens v ON v.cod_pedido = pv.cod_pedido
        INNER JOIN custos_servicos cs ON cs.cod_servico = pv.cod_servico
    where to_char(pv.data_hora_recolha_passageiro, 'd') in (1,3,5,7) 
    
)

select t.nome, t.matricula, t.data, t.hora, t.custo
from tabela t
where t.data > ADD_MONTHS(SYSDATE, -6) and t.hora between '10:00' and '15:00'
order by t.nome ASC, t.custo DESC;

--H--
WITH condutores_viagens_pedidos as
(
select c.nome, pv.nr_idCivil, v.cod_viagem
from condutores c
inner join pedidos_viagens pv on c.nr_idCivil = pv.nr_idcivil
inner join viagens v on pv.cod_pedido = v.cod_pedido
order by 2
)

select *
from condutores_viagens_pedidos cvp
where (select count(cvp.cod_viagem)
	from itinerarios_viagens iv
	where cvp.cod_viagem = iv.cod_viagem) = (select count (distinct iv2.cod_ponto_turistico)
		from itinerarios_viagens iv2);
                                                    
--i--
--Apresente o nome e o nr_idCivil de condutores que fizeram viagens com os mesmos pontos tur�sticos de viagens realizadas 
--pelo menos por 5 outros condutores;
SELECT C.nome, C.nr_idCivil, PV.cod_pedido
FROM condutores C 
    INNER JOIN pedidos_viagens PV ON C.nr_idCivil = PV.nr_idCivil 
    INNER JOIN viagens V ON V.cod_pedido = PV.cod_pedido
WHERE (SELECT COUNT(DISTINCT A.nr_idCivil) 
        FROM (SELECT PV3.nr_idCivil, V3.cod_viagem, PV3.cod_pedido
                FROM pedidos_viagens PV3
                    INNER JOIN viagens V3 ON PV3.cod_pedido = V3.cod_pedido
                WHERE V.cod_viagem = V3.cod_viagem 
                    OR V3.cod_viagem IN (SELECT a.cod_viagem 
                                            FROM itinerarios_viagens a
                                            GROUP BY a.cod_viagem 
                                            HAVING NOT EXISTS (SELECT b.cod_ponto_turistico 
                                                                FROM itinerarios_viagens b
                                                                WHERE V.cod_viagem = b.cod_viagem
                                                                MINUS
                                                                SELECT b.cod_ponto_turistico
                                                                FROM itinerarios_viagens b
                                                                WHERE a.cod_viagem = b.cod_viagem))
                                                AND V.cod_viagem IN (SELECT cod_viagem FROM itinerarios_viagens)) A
        ) >= 5;


                                                                